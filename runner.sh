#!/bin/sh

cd Debug
while [ 1 ]; do
  running=`ps -e | grep -c wsuart`
  echo $running
  if [ $running -eq 0 ]; then
    sleep 10
    wsntty=`grep -l PRODUCT=403/6015 /sys/bus/usb-serial/devices/tty*/../uevent | cut -d/ -f6`
    echo Starting wsuart $wsntty ...
    ./wsuart /dev/$wsntty > /dev/null &
  fi
  sleep 5
done

