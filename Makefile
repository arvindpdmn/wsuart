CC=gcc

apigw: CFLAGS=-DAPI_ENA -DAPI_CACHE_ENA -DGUI_INTERFACE_DB -DRADIO_BAUD_RATE_10000 -DRADIO_CC1101 -DSMS_ENA -O1 -g3 -Wall -fmessage-length=0
gw: CFLAGS=-DGUI_INTERFACE_DB -DRADIO_BAUD_RATE_10000 -DRADIO_CC1101 -DSMS_ENA -O1 -g3 -Wall -fmessage-length=0
apicloud: CFLAGS=-DAPI_ENA -DAPI_CACHE_ENA -DGUI_INTERFACE_DB -DRADIO_BAUD_RATE_10000 -DRADIO_CC1101 -DSMS_ENA -DREMOTE_DB -O1 -g3 -Wall -fmessage-length=0
cloud: CFLAGS=-DGUI_INTERFACE_DB -DRADIO_BAUD_RATE_10000 -DRADIO_CC1101 -DSMS_ENA -DREMOTE_DB -O1 -g3 -Wall -fmessage-length=0

INC=-I"./inc" -I"./inc/imports"
LIBS=-lpthread -lmysqlclient -lm -ljansson -lcurl -L"/usr/local/lib"
OBJDIR=Debug
EXE=Debug/wsuart

SRCS=$(wildcard src/*.c)
OBJS=$(SRCS:%.c=Debug/%.o)
INCS=$(wildcard inc/*)

apigw: odir $(EXE)
	@echo Compiled for gateway storage using API calls.

gw: odir $(EXE)
	@echo Compiled for gateway storage using MySQL DB calls.

apicloud: odir $(EXE)
	@echo Compiled for cloud storage using API calls.

cloud: odir $(EXE)
	@echo Compiled for cloud storage using MySQL DB calls.

$(EXE): $(OBJS)
	$(CC) $(CFLAGS) $(INC) $(OBJS) $(LIBS) -o $@

$(OBJDIR)/%.o : %.c $(INCS)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

odir:
	mkdir -p Debug/src

clean:
	rm -f $(OBJS) $(EXE)
