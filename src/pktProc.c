/*
 * pktProc.c
 *
 *  Created on: Nov 13, 2013
 *      Author: AP
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <gw.h>
#include <dis.h>
#include <mac_defs.h>
#include <logger.h>
#include <util.h>
#include <wsuart.h>
#include <pktSched.h>
#include <pktProc.h>
#ifdef API_ENA
#include <apiaccess.h>
#else
#include <database.h>
#endif
#include <sniffer.h>
#include <main.h>

#ifdef SMS_ENA
#include <sms.h>
#endif

#define NUM_UNEXPECTED_PKTS 12

#ifdef API_ENA
extern CURLM *mainCon;
#else
extern MYSQL *mainCon;
#endif

static UINT8_t txSeqNum = 0;
// TODO static PktStats pktStats;

/*===================================================================
  Packet Format
  -------------
  Header    Short Addr  MAC Addr   RSSI     LQI      Payload
  10 bytes  2 bytes     8 bytes    1 byte   1 byte   n bytes

  Header Format
  -------------
  Msg Type  Flags   Seq No.  Pyld Len   Hdr CRC   Pyld CRC
  2 bytes   1 byte  1 byte   2 bytes    2 bytes   2 bytes
 ------------------------------------------------------------------*/

UINT16_t calcCkSum16(UINT8_t *buff_p, UINT8_t len)
{
   UINT32_t ckSum = 0;

   while (len > 1)
   {
      UINT16_t tmp = *buff_p;
      tmp = (tmp << 8) | (*(buff_p + 1));
      ckSum = ckSum + tmp;
      buff_p += 2;
      len -= 2;
   }

   if (len > 0)
       ckSum += (*buff_p);

   while (ckSum >> 16)
   {
	  ckSum = (ckSum & 0xffff) + (ckSum >> 16);
   }

   return (~ckSum) & 0xffff;
}

void buildMsgHdr(UINT16_t msgType, UINT8_t *buff_p, UINT16_t pyldLen)
{
   // Fields: msgType(2) flags(1) seqNum(1) payloadLen(2) crc(2) payloadCrc(2)

   UINT16_t calcCrc;

   myhtons(buff_p, msgType);

   buff_p[UART_FRAME_HDR_FLAGS_FIELD_OFF] = 0x0;

   buff_p[UART_FRAME_HDR_SEQ_NR_FIELD_OFF] = txSeqNum++;

   myhtons(buff_p + UART_FRAME_HDR_PYLD_LEN_FIELD_OFF, pyldLen);

   calcCrc = calcCkSum16(buff_p, UART_FRAME_HDR_LEN - UART_FRAME_HDR_CRC_FIELD_LEN*2);
   myhtons(buff_p + UART_FRAME_HDR_HDR_CRC_FIELD_OFF, calcCrc);

   if (pyldLen > 0)
       calcCrc = calcCkSum16(buff_p + UART_FRAME_HDR_LEN, pyldLen);
   else
       calcCrc = 0x0;
   myhtons(buff_p + UART_FRAME_HDR_PYLD_CRC_FIELD_OFF, calcCrc);

   return;
}

void printAckFlags(UINT8_t ackFlag)
{
	char flagStr[128];

	flagStr[0] = 0;

	if (ackFlag & UART_HDR_ACK_BM)
		strncat(flagStr, " | HDR",sizeof(flagStr)-strlen(flagStr)-1);

	// TODO Process UART_PYLD_ACK_BM

	if (ackFlag & UART_ACK_STS_OK_BM)
		strncat(flagStr, " | OK",sizeof(flagStr)-strlen(flagStr)-1);

	if (ackFlag & UART_ACK_STS_OOM_BM)
		strncat(flagStr, " | OOM",sizeof(flagStr)-strlen(flagStr)-1);

	if (ackFlag & UART_ACK_STS_FRAME_TOO_LONG_BM)
		strncat(flagStr, " | FRAME_TOO_LONG",sizeof(flagStr)-strlen(flagStr)-1);

	if (ackFlag & UART_ACK_STS_INV_CRC)
		strncat(flagStr, " | INV_CRC",sizeof(flagStr)-strlen(flagStr)-1);

	if (ackFlag & UART_ACK_STS_RELAY_IN_PROGRESS)
		strncat(flagStr, " | RELAY_IN_PROGRESS",sizeof(flagStr)-strlen(flagStr)-1);

	if (ackFlag & UART_ACK_STS_HDR_BYTES_MISSING)
		strncat(flagStr, " | HDR_BYTES_MISSING",sizeof(flagStr)-strlen(flagStr)-1);

	if (ackFlag & UART_ACK_STS_PYLD_BYTES_MISSING)
		strncat(flagStr, " | PYLD_BYTES_MISSING",sizeof(flagStr)-strlen(flagStr)-1);

	if (strlen(flagStr)>2) {
		LOG(LOG_DBG_HIGH, "%s", flagStr+2); // ignore the first " |"
	}
	else {
		LOG(LOG_DBG_HIGH, " -");
	}
}

void printEventType(UINT8_t eventType)
{
	char eventStr[32];

	eventStr[0] = 0;

	switch (eventType)
	{
		case LPWMN_GW_EVT_TYPE_NODE_REG:
			strncat(eventStr, " NODE_REG", sizeof(eventStr)-1);
			break;
		case LPWMN_GW_EVT_TYPE_LOCAL_RT_DISC_STARTED:
			strncat(eventStr, " RT_DISC_STARTED", sizeof(eventStr)-1);
			break;
		case LPWMN_GW_EVT_TYPE_SYS_BOOT:
			strncat(eventStr, " SYS_BOOT", sizeof(eventStr)-1);
			break;
		case LPWMN_GW_EVT_TYPE_NODE_DEREG:
			strncat(eventStr, " NODE_DEREG", sizeof(eventStr)-1);
			break;
		case LPWMN_GW_EVT_TYPE_LOCAL_RT_DISC_OVER:
			strncat(eventStr, " RT_DISC_OVER", sizeof(eventStr)-1);
			break;
		case LPWMN_GW_EVT_TYPE_BCN_REQ_RCVD:
			strncat(eventStr, " BCN_REQ_RCVD", sizeof(eventStr)-1);
			break;
		case LPWMN_GW_EVT_TYPE_MAC_CNFRM:
			strncat(eventStr, " MAC_CNFRM", sizeof(eventStr)-1);
			break;
		case LPWMN_GW_EVT_TYPE_PURGE_RT_TBL:
			strncat(eventStr, " PURGE_RT_TBL", sizeof(eventStr)-1);
			break;
		default:
			strncat(eventStr, " UNKNOWN", sizeof(eventStr)-1);
			break;
	}

	LOG(LOG_DBG_HIGH, "%s", eventStr);
}

const char* getHdrTypeStr(UINT16_t pktType)
{
	switch (pktType) {
		case UART_MSG_TYPE_ACK:
			return "ACK";
		case LPWMN_GW_MSG_TYPE_NODE_CNT_REQ:
			return "NODE_CNT_REQ";
		case LPWMN_GW_MSG_TYPE_NODE_LIST_REQ:
			return "NODE_LIST_REQ";
		case LPWMN_GW_MSG_TYPE_GET_NODE_LAST_FATAL_ERR_ID:
			return "NODE_LAST_FATAL_ERR_ID";
		case LPWMN_GW_MSG_TYPE_RELAY_TO_NODE:
			return "RELAY_TO_NODE";
		case LPWMN_GW_MSG_TYPE_RELAY_FROM_NODE:
			return "RELAY_FROM_NODE";
		case LPWMN_GW_MSG_TYPE_KEEP_ALIVE_REQ:
			return "KEEP_ALIVE_REQ";
		case LPWMN_GW_MSG_TYPE_KEEP_ALIVE_RESP:
			return "KEEP_ALIVE_RESP";
		case LPWMN_GW_MSG_TYPE_PING_NODE_REQ:
			return "PING_NODE_REQ";
		case LPWMN_GW_MSG_TYPE_NODE_PING_RESP:
			return "NODE_PING_RESP";
		case LPWMN_GW_MSG_TYPE_NODE_RT_DISC_STS:
			return "NODE_RT_DISC_STS";
		case LPWMN_GW_MSG_TYPE_PATH_DISC_REQ:
			return "PATH_DISC_REQ";
		case LPWMN_GW_MSG_TYPE_PATH_DISC_RESP:
			return "PATH_DISC_RESP";
		case LPWMN_GW_MSG_TYPE_NODE_TRAFFIC_STATS_REQ:
			return "TRAFFIC_STATS_REQ";
		case LPWMN_GW_MSG_TYPE_RESET_NODE_TRAFFIC_STATS:
			return "NODE_TRAFFIC_STATS";
		case LPWMN_GW_MSG_TYPE_STOP_TRAFFIC_REQ:
			return "STOP_TRAFFIC_REQ";
		case LPWMN_GW_MSG_TYPE_START_TRAFFIC_REQ:
			return "START_TRAFFIC_REQ";
		case LPWMN_GW_MSG_TYPE_GET_COORD_STATS:
			return "GET_COORD_STATS";
		case LPWMN_GW_MSG_TYPE_RESET_COORD_STATS:
			return "RESET_COORD_STATS";
		case LPWMN_GW_MSG_TYPE_STOP_NWK:
			return "STOP_NWK";
		case LPWMN_GW_MSG_TYPE_START_NWK:
			return "START_NWK";
		case LPWMN_GW_MSG_TYPE_ENABLE_NWK_JOINS:
			return "ENABLE_NWK_JOINS";
		case LPWMN_GW_MSG_TYPE_DISABLE_NWK_JOINS:
			return "DISABLE_NWK_JOINS";
		case LPWMN_GW_MSG_TYPE_GET_NWK_JOIN_CTRL_STATE:
			return "GET_NWK_JOIN_CTRL_STATE";
		case LPWMN_GW_MSG_TYPE_ADD_NODE_TO_WHITE_LIST:
			return "ADD_NODE_TO_WHITE_LIST";
		case LPWMN_GW_MSG_TYPE_DEL_NODE_FROM_WHITE_LIST:
			return "DEL_NODE_FROM_WHITE_LIST";
		case LPWMN_GW_MSG_TYPE_DELETE_WHITE_LIST:
			return "DELETE_WHITE_LIST";
		case LPWMN_GW_MSG_TYPE_GET_WHITE_LIST_ENTRY:
			return "GET_WHITE_LIST_ENTRY";
		case LPWMN_GW_MSG_TYPE_SET_NWK_LPWMN_ID:
			return "NWK_PAN_ID";
		case LPWMN_GW_MSG_TYPE_SET_RADIO_CHANNEL:
			return "SET_RADIO_CHANNEL";
		case LPWMN_GW_MSG_TYPE_SET_LPWMN_COORD_EXT_ADDR:
			return "SET_PAN_COORD_EXT_ADDR";
		case LPWMN_GW_MSG_TYPE_SET_RADIO_TX_PWR:
			return "SET_RADIO_TX_PWR";
		case LPWMN_GW_MSG_TYPE_START_COORD:
			return "START_COORD";
		case LPWMN_GW_MSG_TYPE_STOP_COORD:
			return "STOP_COORD";
		case LPWMN_GW_MSG_TYPE_REBOOT_COORD:
			return "REBOOT_COORD";
		case LPWMN_GW_MSG_TYPE_GET_NWK_LPWMN_ID:
			return "GET_NWK_LPWMN_ID";
		case LPWMN_GW_MSG_TYPE_GET_NWK_CHANNEL:
			return "GET_NWK_CHANNEL";
		case LPWMN_GW_MSG_TYPE_GET_LPWMN_COORD_EXT_ADDR:
			return "GET_LPWMN_COORD_EXT_ADDR";
		case LPWMN_GW_MSG_TYPE_GET_RADIO_TX_PWR:
			return "GET_RADIO_TX_PWR";
		case LPWMN_GW_MSG_TYPE_GET_RADIO_PART_NR:
			return "GET_RADIO_PART_NR";
		case LPWMN_GW_MSG_TYPE_GET_RADIO_FREQ_BAND:
			return "GET_FREQ_BAND";
		case LPWMN_GW_MSG_TYPE_GET_CARRIER_FREQ:
			return "GET_CARRIER";
		case LPWMN_GW_MSG_TYPE_GET_RADIO_MOD_FMT:
			return "GET_RADIO_MOD_FMT";
		case LPWMN_GW_MSG_TYPE_GET_RADIO_BAUD_RATE:
			return "GET_RADIO_BAUD";
		case LPWMN_GW_MSG_TYPE_GET_COORD_SW_BUILD_DATE:
			return "GET_COORD_SW_BUILD_DATE";
		case LPWMN_GW_MSG_TYPE_GET_COORD_SW_BUILD_TIME:
			return "GET_COORD_SW_BUILD_TIME";
		case LPWMN_GW_MSG_TYPE_SET_WCT:
			return "SET_WCT";
		case LPWMN_GW_MSG_TYPE_GET_WCT:
			return "GET_WCT";
		case LPWMN_MSG_TYPE_GET_UP_TIME:
			return "GET_UP_TIME";
		case LPWMN_GW_MSG_TYPE_COORD_LOG:
			return "COORD_LOG";
		case LPWMN_GW_MSG_TYPE_ENABLE_COORD_LOG:
			return "ENABLE_COORD_LOG";
		case LPWMN_GW_MSG_TYPE_DISABLE_COORD_LOG:
			return "DISABLE_COORD_LOG";
		case LPWMN_GW_MSG_TYPE_DEREG_NODE:
			return "DEREG_NODE";
		case LPWMN_GW_MSG_TYPE_EVENT:
			return "EVENT";
		default:
			return "UNKNOWN";
	}
}

void printPacket(UINT8_t *pkt, const char *descSuffix, int onlyHdr, int isRxPkt)
{
	UINT16_t pktType = myntohs(pkt);
	UINT16_t pyldLen = myntohs(pkt+UART_FRAME_HDR_PYLD_LEN_FIELD_OFF);

	char *typeStr;
	UINT8_t buffLen = strlen(descSuffix)+32+32;
	typeStr = malloc(buffLen);

	if (isRxPkt) strcpy(typeStr, "--> ");
	else strcpy(typeStr, "<-- ");

	strncat(typeStr, getHdrTypeStr(pktType), buffLen);
	if (strstr(typeStr,"UNKNOWN")) pyldLen = 0; // print only header

	time_t now;
	struct tm *info;
	char nowStr[32];
	time(&now);
	info = localtime(&now);
	strftime(nowStr, sizeof(nowStr), "%T", info);   
	sprintf(typeStr+strlen(typeStr), " [%s] [%s]", descSuffix, nowStr);
	if (onlyHdr)
		PRINT_BUFFER(LOG_DBG_HIGH, typeStr, pkt, UART_FRAME_HDR_LEN);
	else
		PRINT_BUFFER(LOG_DBG_HIGH, typeStr, pkt, UART_FRAME_HDR_LEN+pyldLen);

	if (pktType == UART_MSG_TYPE_ACK) {
		printAckFlags(pkt[UART_FRAME_HDR_FLAGS_FIELD_OFF]);
	}
	else if (pktType == LPWMN_GW_MSG_TYPE_EVENT) {
		printEventType(pkt[UART_FRAME_HDR_LEN]);
	}

	free(typeStr);
}

UINT16_t getRxPayloadSize(UINT16_t type, UINT8_t evtType)
{
	UINT16_t size = 0;

	switch (type) {
		case LPWMN_GW_MSG_TYPE_EVENT:
			switch (evtType) {
			case LPWMN_GW_EVT_TYPE_NODE_REG:
			case LPWMN_GW_EVT_TYPE_NODE_DEREG:
				// eventType(1)+shortAddr(2)+macAddr(8)+macCapInfo(1)
				size = 12;
				break;
			case LPWMN_GW_EVT_TYPE_LOCAL_RT_DISC_STARTED:
				// eventType(1)+shortAddr(2)+attemptNum(2)
				size = 5;
				break;
			case LPWMN_GW_EVT_TYPE_SYS_BOOT:
				// eventType(1)
				size = 1;
				break;
			case LPWMN_GW_EVT_TYPE_LOCAL_RT_DISC_OVER:
				// eventType(1)+destAddr(2)+nextHop(2)
				size = 5;
				break;
			case LPWMN_GW_EVT_TYPE_BCN_REQ_RCVD:
				// eventType(1)+bcnReqCount(2)
				size = 3;
				break;
			case LPWMN_GW_EVT_TYPE_MAC_CNFRM:
				// eventType(1)+macCfmCount(2)+macStatus(1)+srcModule(1)
				size = 5;
				break;
			case LPWMN_GW_EVT_TYPE_PURGE_RT_TBL:
				// eventType(1)+shortAddr(2)
				size = 3;
				break;
			}
			break;
		case LPWMN_GW_MSG_TYPE_GET_NWK_JOIN_CTRL_STATE:
		case LPWMN_GW_MSG_TYPE_SET_RADIO_CHANNEL:
		case LPWMN_GW_MSG_TYPE_START_COORD:
			size = 1;
			break;
		case LPWMN_GW_MSG_TYPE_GET_NWK_LPWMN_ID:
		case LPWMN_GW_MSG_TYPE_GET_NWK_CHANNEL:
		case LPWMN_GW_MSG_TYPE_GET_RADIO_TX_PWR:
		case LPWMN_GW_MSG_TYPE_NODE_CNT_REQ:
		case LPWMN_GW_MSG_TYPE_GET_NODE_LAST_FATAL_ERR_ID:
			size = 2;
			break;
		case LPWMN_GW_MSG_TYPE_GET_RADIO_BAUD_RATE:
			size = 4;
			break;
		case LPWMN_GW_MSG_TYPE_GET_LPWMN_COORD_EXT_ADDR:
			size = 8;
			break;
		case LPWMN_GW_MSG_TYPE_NODE_LIST_REQ: // 17 or 2 bytes
			// nodeIndex(2)+shortAddr(2)+parentShortAddr(2)+macAddr(8)+macCapInfo(1)+lastPktTs(2)
			// payload of 2 bytes is sent when an invalid node index is requested
		case LPWMN_GW_MSG_TYPE_GET_WHITE_LIST_ENTRY: // 10 or 2 bytes
			// nodeIndex(2)+macAddr(8)
			// payload of 2 bytes is sent when an invalid node index is requested
		case LPWMN_GW_MSG_TYPE_GET_RADIO_FREQ_BAND:
		case LPWMN_GW_MSG_TYPE_GET_RADIO_MOD_FMT:
		case LPWMN_GW_MSG_TYPE_GET_COORD_SW_BUILD_DATE:
		case LPWMN_GW_MSG_TYPE_GET_COORD_SW_BUILD_TIME:
			// payloads of variable sizes
			size = VAR_PYLD_SIZE;
			break;
		default:
			size = 0;
			break;
	}

	return size;
}

UINT8_t isExpectedPyldLen(UINT8_t *rxPkt)
{
	UINT16_t rxMsgType = myntohs(rxPkt);
	UINT8_t eventType = 0;
	if (rxMsgType == LPWMN_GW_MSG_TYPE_EVENT) {
		eventType = rxPkt[UART_FRAME_HDR_LEN];
	}

	UINT16_t pyldSize = getRxPayloadSize(rxMsgType, eventType);
	UINT16_t rxPyldSize = myntohs(rxPkt+UART_FRAME_HDR_PYLD_LEN_FIELD_OFF);
	if (pyldSize == rxPyldSize ||
		pyldSize == VAR_PYLD_SIZE ||
		// multiple fixed sizes
		((rxPyldSize == 2 || rxPyldSize == 10) &&
			rxMsgType == LPWMN_GW_MSG_TYPE_GET_WHITE_LIST_ENTRY) ||
		((rxPyldSize == 2 || rxPyldSize == 10) &&
			rxMsgType == LPWMN_GW_MSG_TYPE_GET_WHITE_LIST_ENTRY)) {
		return 1;
	}

	// TODO Send an NACK when payload length is not as expected
	LOG(LOG_WARN, "Payload length is of unexpected value for message 0x%04X: [expected,actual]: [%d, %d]",
			rxMsgType, pyldSize, rxPyldSize);

	return 0; // not expected payload length
}

void sendHdr(UINT16_t type, UINT8_t *payload, UINT8_t pyldLen)
{
	UINT8_t *buffer;
	UINT16_t pktSize = UART_FRAME_HDR_LEN + pyldLen;

	buffer = malloc(pktSize);
	memset(buffer, 0, pktSize);

	if (payload) {
		memcpy(buffer+UART_FRAME_HDR_LEN, payload, pyldLen);
	}
	// else: send zeros of pyldlen bytes

	buildMsgHdr(type, buffer, pyldLen);
	printPacket(buffer, "TxHdr", 1, 0);

	writePort(buffer, UART_FRAME_HDR_LEN);

	free(buffer);
}

UINT8_t* receivePkt(void)
{
	UINT8_t hdrBuffer[UART_FRAME_HDR_LEN];
	UINT16_t chkSum, rxChkSum;

	// Receive header
	memset(hdrBuffer, 0, sizeof(hdrBuffer));
	if (readPort(hdrBuffer, UART_FRAME_HDR_LEN) <= 0) {
		return NULL;
	}

	// Verify checksum on header
	chkSum = calcCkSum16(hdrBuffer, UART_FRAME_HDR_LEN - UART_FRAME_HDR_CRC_FIELD_LEN*2);
	rxChkSum = myntohs(hdrBuffer + UART_FRAME_HDR_HDR_CRC_FIELD_OFF);
	if (rxChkSum != chkSum) {
       printPacket(hdrBuffer, "RxHdrCrcKO", 1, 1);
	   LOG(LOG_DBG_MEDIUM, "Received header checksum is not matching: [rxd,calc]: [%04x,%04x]",
			   rxChkSum, chkSum);
	   return NULL;
	}

	// Receive payload
	UINT8_t *buffer;
	UINT16_t payloadSize = myntohs(hdrBuffer + UART_FRAME_HDR_PYLD_LEN_FIELD_OFF);
	if (payloadSize == 0) {
		// only header is present, no payload
		buffer = malloc(UART_FRAME_HDR_LEN);
		memcpy(buffer, hdrBuffer, UART_FRAME_HDR_LEN);
		printPacket(hdrBuffer, "RxHdrCrcOK", 0, 1);
		return buffer;
	}
	else {
		// TODO Is the payload length correct for the message type?
		buffer = malloc(UART_FRAME_HDR_LEN + payloadSize);
		memset(buffer, 0, UART_FRAME_HDR_LEN + payloadSize);
		memcpy(buffer, hdrBuffer, UART_FRAME_HDR_LEN);
		if (readPort(buffer + UART_FRAME_HDR_LEN, payloadSize) <= 0) {
			return NULL;
		}
	}

	// Verify checksum on payload
	chkSum = calcCkSum16(buffer+UART_FRAME_HDR_LEN, payloadSize);
	rxChkSum = myntohs(buffer + UART_FRAME_HDR_PYLD_CRC_FIELD_OFF);
	if (rxChkSum != chkSum) {
		printPacket(buffer, "RxPktCrcKO", 0, 1);
		LOG(LOG_DBG_MEDIUM, "Received payload checksum is not matching: [rxd,calc]: [%04x,%04x]",
			   rxChkSum, chkSum);
		free(buffer);
		return NULL;
	}

	printPacket(buffer, "RxPkt", 0, 1);
	return buffer;
}

UINT8_t* txHdrRxPkt(UINT16_t type, UINT8_t retries)
{
	UINT8_t i, otherPkts;
	UINT8_t *rxPkt = NULL;
	UINT16_t rxMsgType;

	// For REQ that have no payload:
	// Send header in REQ, wait for header and payload in RSP
	for (i=0; i<=retries; i++)
	{
		LOG(LOG_DBG_MEDIUM, "Retry number %d ...", i);
		sendHdr(type, NULL, 0);
		otherPkts = 0;

		// Return if no payload response is expected
		if (getRxPayloadSize(type,0)==0) return NULL;

		while (1) {
			FREE(rxPkt);
			rxPkt = receivePkt();
			if (rxPkt == NULL) {
				// timed out as nothing was received
				break; // go into retry
			}
			else {
				// something has been received
				rxMsgType = myntohs(rxPkt);
				if (rxMsgType == type) {
					// accept only the expected packet
					if (isExpectedPyldLen(rxPkt))
						return rxPkt;
					else {
						// TODO Alternatively send an NACK or log the error and then retry
						break; // go into retry
					}
				}
				else {
					// right away process an unexpected packet
					if (NwInitDone) processPkt(NULL, rxPkt);
					if (++otherPkts > NUM_UNEXPECTED_PKTS) {
						break; // go into retry
					}
				}
			}
		}
	}

	FREE(rxPkt);
	return NULL;
}

UINT8_t* txPktRxPkt(UINT16_t type, UINT8_t *payload, UINT8_t pyldLen, UINT8_t retries)
{
	UINT8_t i, j, otherPkts;
	UINT8_t *rxPkt = NULL;
	UINT16_t seqNum;
	UINT16_t rxMsgType;

	// For REQ that have payload:
	// Send header in REQ, wait for ACK, send payload, wait for header and payload in RSP
	for (i=0; i<=retries; i++)
	{
		LOG(LOG_DBG_MEDIUM, "Retry number %d ...", i);
		seqNum = txSeqNum;
		sendHdr(type, payload, pyldLen);
		otherPkts = 0;
		while (1) {
			FREE(rxPkt);
			rxPkt = receivePkt();
			if (rxPkt == NULL) {
				// timed out as nothing was received
				break; // go into retry
			}
			else {
				rxMsgType = myntohs(rxPkt);
				if (rxMsgType != UART_MSG_TYPE_ACK) {
					// right away process an unexpected packet
					if (NwInitDone) processPkt(NULL, rxPkt);
					if (++otherPkts > NUM_UNEXPECTED_PKTS) {
						break; // go into retry
					}
				}
				else {
					// accept only the expected packet
					if (UART_HDR_ACK_BM & rxPkt[UART_FRAME_HDR_FLAGS_FIELD_OFF] &&
						UART_ACK_STS_OK_BM & rxPkt[UART_FRAME_HDR_FLAGS_FIELD_OFF] &&
						seqNum == rxPkt[UART_FRAME_HDR_SEQ_NR_FIELD_OFF]) {
						LOG(LOG_DBG_MEDIUM, "ACK received OK.");
					}
					else if (UART_ACK_STS_PYLD_BYTES_MISSING & rxPkt[UART_FRAME_HDR_FLAGS_FIELD_OFF] &&
						seqNum == rxPkt[UART_FRAME_HDR_SEQ_NR_FIELD_OFF]) {
						// TODO Test UART_ACK_STS_PYLD_BYTES_MISSING received flag
						LOG(LOG_DBG_MEDIUM, "Moving to payload Tx.");
					}
					else {
						LOG(LOG_DBG_MEDIUM, "ACK received KO.");
						break; // go into retry
					}

					for (j=i; j<=retries; j++) {
						PRINT_BUFFER(LOG_DBG_HIGH, "<-- [TxPyld]", payload, pyldLen);
						writePort(payload, pyldLen);
						otherPkts = 0;

						// Return if no payload response is expected
						if (getRxPayloadSize(type,0)==0) {
							FREE(rxPkt);
							return NULL;
						}

						while (1) {
							FREE(rxPkt);
							rxPkt = receivePkt();
							if (rxPkt == NULL) {
								// timed out as nothing was received
								break; // go into retry
							}
							else { // something has been received
								rxMsgType = myntohs(rxPkt);
								if (UART_ACK_STS_PYLD_BYTES_MISSING & rxPkt[UART_FRAME_HDR_FLAGS_FIELD_OFF] &&
									seqNum == rxPkt[UART_FRAME_HDR_SEQ_NR_FIELD_OFF]) {
									// payload got lost: target receiver timed out
									j = retries+1;
									break; // go into retry from the header since target clears its state
								}
								else if (UART_ACK_STS_HDR_BYTES_MISSING & rxPkt[UART_FRAME_HDR_FLAGS_FIELD_OFF] &&
									seqNum == rxPkt[UART_FRAME_HDR_SEQ_NR_FIELD_OFF]) {
									// target received payload, sent response that got lost, host resent payload
									j = retries+1;
									break; // go into retry from the header
								}
								else if (rxMsgType != type) {
									if (NwInitDone) processPkt(NULL, rxPkt);
									if (++otherPkts > NUM_UNEXPECTED_PKTS) {
										break; // go into retry of payload
									}
								}
								else {
									// accept only the expected packet
									if (isExpectedPyldLen(rxPkt))
										return rxPkt;
									else {
										// TODO Alternatively send an NACK and then retry
										j = retries+1;
										break; // go into retry from the header
									}
								}
							}
						}
					} // end of for
				}
			}
		}
	}

	FREE(rxPkt);
	return NULL;
}

UINT8_t processPkt(void *ret_p, UINT8_t *rxPkt)
{
	// Function updates ret_p only when there are no errors

	if (rxPkt == NULL) return 1;

	UINT32_t val32, *val32_p = ret_p;
	UINT16_t val16, *val16_p = ret_p;
	UINT8_t val8, *val8_p = ret_p;
	UINT8_t retVal = 0;

	UINT8_t data[STR_LEN];
	if (ret_p==NULL) ret_p = data;

	UINT16_t rxMsgType = myntohs(rxPkt);
	UINT16_t payloadSize = myntohs(rxPkt + UART_FRAME_HDR_PYLD_LEN_FIELD_OFF);

	char *coordMacAddrStr = NULL;
	UINT8_t *coordMacAddr = NULL;

	switch (rxMsgType)
	{
		case LPWMN_GW_MSG_TYPE_GET_NWK_LPWMN_ID:
			// TODO Schedule other network info if Pan Id changes
			val16 = myntohs(rxPkt+UART_FRAME_HDR_LEN);
			retVal = updatePanId(mainCon, val16);
			if (val16_p && retVal==0) *val16_p = val16;
			break;
		case LPWMN_GW_MSG_TYPE_NODE_CNT_REQ:
			val16 = myntohs(rxPkt+UART_FRAME_HDR_LEN);
			retVal = updateNodeCount(mainCon, val16);
			if (val16_p && retVal==0) *val16_p = val16;
			break;
		case LPWMN_GW_MSG_TYPE_GET_RADIO_FREQ_BAND:
			memset(ret_p, 0, STR_LEN);
			strncpy(ret_p, (char *)rxPkt+UART_FRAME_HDR_LEN, STR_LEN-1);
			retVal = updateBand(mainCon, ret_p);
			break;
		case LPWMN_GW_MSG_TYPE_GET_NWK_CHANNEL:
			val32 = BASE_FREQ + CHANNEL_SPACING*myntohs(rxPkt+UART_FRAME_HDR_LEN); // save as Hz
			retVal = updateNwChannel(mainCon, val32);
			if (val32_p && retVal==0) *val32_p = val32;
			break;
		case LPWMN_GW_MSG_TYPE_GET_RADIO_MOD_FMT:
			memset(ret_p, 0, STR_LEN);
			strncpy(ret_p, (char *)rxPkt+UART_FRAME_HDR_LEN, STR_LEN-1);
			retVal = updateModulation(mainCon, ret_p);
			break;
		case LPWMN_GW_MSG_TYPE_GET_RADIO_BAUD_RATE:
			val32 = myntohl(rxPkt+UART_FRAME_HDR_LEN);
			retVal = updateRadioBaudRate(mainCon, val32);
			if (val32_p && retVal==0) *val32_p = val32;
			break;
		case LPWMN_GW_MSG_TYPE_GET_LPWMN_COORD_EXT_ADDR:
			retVal = updateCoordExtAddr(mainCon, rxPkt+UART_FRAME_HDR_LEN);
			if (ret_p && retVal==0) memcpy(ret_p,rxPkt+UART_FRAME_HDR_LEN,8);
			break;
		case LPWMN_GW_MSG_TYPE_GET_RADIO_TX_PWR:
			val16 = myntohs(rxPkt+UART_FRAME_HDR_LEN);
			retVal = updateCoordTxPwr(mainCon, val16);
			if (val16_p && retVal==0) *val16_p = val16;
			break;
		case LPWMN_GW_MSG_TYPE_GET_NWK_JOIN_CTRL_STATE:
			val8 = rxPkt[UART_FRAME_HDR_LEN];
			retVal = updateNodeAssocState(mainCon, val8);
			if (val8_p && retVal==0) *val8_p = val8;
			break;
		case LPWMN_GW_MSG_TYPE_GET_COORD_SW_BUILD_DATE:
			memset(ret_p, 0, STR_LEN);
			strncpy(ret_p, (char *)rxPkt+UART_FRAME_HDR_LEN, STR_LEN-1);
			coordMacAddrStr = readCoordExtAddr(mainCon);
			coordMacAddr = parseString(coordMacAddrStr);
			retVal = updateNodeBuildDate(mainCon, coordMacAddr, ret_p);
			free(coordMacAddrStr);
			free(coordMacAddr);
			break;
		case LPWMN_GW_MSG_TYPE_GET_COORD_SW_BUILD_TIME:
			memset(ret_p, 0, STR_LEN);
			strncpy(ret_p, (char *)rxPkt+UART_FRAME_HDR_LEN, STR_LEN-1);
			coordMacAddrStr = readCoordExtAddr(mainCon);
			coordMacAddr = parseString(coordMacAddrStr);
			retVal = updateNodeBuildTime(mainCon, coordMacAddr, ret_p);
			free(coordMacAddrStr);
			free(coordMacAddr);
			break;
		case LPWMN_GW_MSG_TYPE_GET_WHITE_LIST_ENTRY:
			if (val8_p) {
				if (payloadSize==2) memset(val8_p, 0xFF, 8);
				else if (payloadSize==10) memcpy(val8_p, rxPkt+UART_FRAME_HDR_LEN+2, 8);
				else retVal = 1;
			}
			break;
		case LPWMN_GW_MSG_TYPE_RELAY_FROM_NODE:
			retVal = processRelayPkt(rxPkt);
			break;
		case LPWMN_GW_MSG_TYPE_EVENT:
			retVal = processEvent(rxPkt);
			break;
		default: // TODO Add default processing if any
			break;
	}

	return retVal;
}

const char* getDevName(UINT8_t id)
{
	switch (id)
	{
		case PLTFRM_AT24C01C_1_DEV_ID: return "AT24C01C_1";
		case PLTFRM_AT24MAC602_1_DEV_ID: return "AT24MAC602_1";
		case PLTFRM_M24M01_1_DEV_ID: return "M24M01_1";
		case PLTFRM_LM75B_1_DEV_ID: return "LM75B_1";
		case PLTFRM_TMP102_1_DEV_ID: return "TMP102_1";
		case PLTFRM_NTC_THERMISTOR_1_DEV_ID: return "NTC_THERMISTOR_1";
		case PLTFRM_NTC_THERMISTOR_2_DEV_ID: return "NTC_THERMISTOR_2";
		case PLTFRM_SFH_7773_1_DEV_ID: return "SFH_7773_1";
		case PLTFRM_TSL45315_1_DEV_ID: return "TSL45315_1";
		case PLTFRM_MPU6050_1_DEV_ID: return "MPU6050_1";
		case PLTFRM_ADXL345_1_DEV_ID: return "ADXL345_1";
		case PLTFRM_SHT10_1_DEV_ID: return "SHT10_1";
		case PLTFRM_LIS3MDLTR_1_DEV_ID: return "LIS3MDLTR_1";
		case PLTFRM_CC2520_1_DEV_ID: return "CC2520_1";
		case PLTFRM_CC1200_1_DEV_ID: return "CC1200_1";
		case PLTFRM_CC1101_1_DEV_ID: return "CC1101_1";
		case PLTFRM_CC3100_1_DEV_ID: return "CC3100_1";
		case PLTFRM_MPL115A1_1_DEV_ID: return "MPL115A1_1";
		case PLTFRM_MAX_SONAR_1_DEV_ID: return "MAX_SONAR_1";
		case PLTFRM_EKMC160111X_1_DEV_ID: return "EKMC160111X_1";
		case PLTFRM_LED_1_DEV_ID: return "LED_1";
		case PLTFRM_LED_2_DEV_ID: return "LED_2";
		case PLTFRM_REED_SWITCH_1_DEV_ID: return "REED_SWITCH_1";
		case PLTFRM_SPST_SWITCH_1_DEV_ID: return "SPST_SWITCH_1";
		case PLTFRM_MP3V5050GP_1_DEV_ID: return "MP3V5050GP_1";
		case PLTFRM_HE055T01_1_DEV_ID: return "HE055T01_1";
		case PLTFRM_MP3V5004GP_1_DEV_ID: return "MP3V5004GP_1";
		case PLTFRM_LLS_1_DEV_ID: return "LLS_1";
		case PLTFRM_BATT_1_DEV_ID: return "BATT_1";
		case PLTFRM_AD7797_1_DEV_ID: return "AD7797_1";
		case PLTFRM_ON_CHIP_VCC_SENSOR_DEV_ID: return "ON_CHIP_VCC_SENSOR";
		case PLTFRM_ON_CHIP_TEMP_SENSOR_DEV_ID: return "ON_CHIP_TEMP_SENSOR";
		case PLTFRM_SYNC_RT_1_DEV_ID: return "SYNC_RT_1";
		case PLTFRM_CC2D33S_1_DEV_ID: return "CC2D33S_1";
		case PLTFRM_GPIO_REMOTE_CTRL_DEV_ID: return "GPIO_REMOTE_CTRL";
		case PLTFRM_CHIRP_PWLA_1_DEV_ID: return "CHIRP_PWLA_1";
		case PLTFRM_FC_28_1_DEV_ID: return "FC_28_1";
		case PLTFRM_WSMS100_1_DEV_ID: return "WSMS100_1";
		case PLTFRM_MPL3115A2_1_DEV_ID: return "MPL3115A2_1";
		case PLTFRM_MPL115A2_1_DEV_ID: return "MPL115A2_1";
		case PLTFRM_BMP180_1_DEV_ID: return "BMP180_1";
		case PLTFRM_MMA7660FC_1_DEV_ID: return "MMA7660FC_1";
		case PLTFRM_I2C_SW_BUS_1_DEV_ID: return "I2C_SW_BUS_1";
		case PLTFRM_I2C_SW_BUS_2_DEV_ID: return "I2C_SW_BUS_2";
		case PLFRM_SPI_HW_BUS_1_DEV_ID: return "SPI_HW_BUS_1";
		case PLTFRM_32KHZ_CRYSTAL_DEV_ID: return "32KHZ_CRYSTAL";
		case PLTFRM_GENERIC_DEV_ID: return "GENERIC";
		case PLTFRM_UART_HW_1_DEV_ID: return "UART_HW_1";
		default: return "UNKNOWN";
	}
}

const char* getScale(UINT8_t id)
{
	switch (id)
	{
		case DIS_DATA_SCALE_TERA: return "Tera";
		case DIS_DATA_SCALE_GIGA: return "Giga";
		case DIS_DATA_SCALE_MEGA: return "Mega";
		case DIS_DATA_SCALE_KILO: return "Kilo";
		case DIS_DATA_SCALE_HECTO: return "Hecto";
		case DIS_DATA_SCALE_DEKA: return "Deka";
		case DIS_DATA_SCALE_NONE: return "-";
		case DIS_DATA_SCALE_DECI: return "deci";
		case DIS_DATA_SCALE_CENTI: return "centi";
		case DIS_DATA_SCALE_MILLI: return "milli";
		case DIS_DATA_SCALE_MICRO: return "micro";
		case DIS_DATA_SCALE_NANO: return "nano";
		case DIS_DATA_SCALE_PICO: return "pico";
		case DIS_DATA_SCALE_FEMTO: return "femto";
		default: return "???";
	}
}

void ruleNotify(UINT8_t *macAddr, char *macAddrStr, SensorData *sdata_p, UINT32_t convertedVal,
	unsigned int ruleUid, const char *type, const char *threshold, const char *actionOn, const char *mobileNum)
{
	int toNotify = 0;
	char strBuff[128];

	LOG(LOG_DBG_MEDIUM, "ruleNotify(-, %s, -, %lu, %d, %s, %s, %s, %s)", macAddrStr, convertedVal, ruleUid, type, threshold, actionOn, mobileNum);

	if (strcmp(type,"LowThreshold")==0 && convertedVal<atoi(threshold)) {
		sprintf(strBuff, "%s; MacAddr=0x%s; Sensor=%s; Below low threshold: %d < %s.",
				actionOn, macAddrStr, getDevName(sdata_p->type), (int)convertedVal, threshold);
		insertEvent(mainCon, macAddr, "Warn", "RuleTrigger", strBuff);
		toNotify = 1;
	}
	else if (strcmp(type,"HighThreshold")==0 && convertedVal>atoi(threshold)) {
		sprintf(strBuff, "%s; MacAddr=0x%s; Sensor=%s; High threshold exceeded: %d > %s.",
				actionOn, macAddrStr, getDevName(sdata_p->type), (int)convertedVal, threshold);
		insertEvent(mainCon, macAddr, "Warn", "RuleTrigger", strBuff);
		toNotify = 1;
	}
	if (toNotify) {
		updateRuleNotifiedTs(mainCon, ruleUid);
#ifdef SMS_ENA
		if (strcmp(mobileNum,""))
			GSM_sendSMS(strBuff, mobileNum);
#endif
	}
}

UINT8_t decodeTlv(UINT8_t *numDataStore, SensorDataStore *ds, UINT8_t *macAddr, char *macAddrStr, UINT8_t *data_p, SensorData *sdata_p)
{
	UINT8_t len = data_p[1];
	UINT8_t offset = 0;
	UINT8_t freeMem = 0;
	static UINT8_t onChipVccMacAddr[8];
	static UINT32_t onChipVccValue;
	UINT32_t sensorVal;
	
	if (sdata_p)
		LOG(LOG_DBG_MEDIUM, "decodeTlv(%d, -, %s, -, NON-NULL): type[%d]", *numDataStore, macAddrStr, data_p[0]);
	else
		LOG(LOG_DBG_MEDIUM, "decodeTlv(%d, -, %s, -, NULL): type[%d]", *numDataStore, macAddrStr, data_p[0]);

	switch (data_p[0])
	{
		case DIS_TLV_TYPE_SENSOR_OUTPUT_LIST:
			*numDataStore = 0;
			while (offset+2<len) {
				offset += decodeTlv(numDataStore, ds, macAddr, macAddrStr, data_p+2+offset, NULL);
			}
			offset += 2;
			break;
		case DIS_TLV_TYPE_SENSOR_OUTPUT:
			printf("Sensor Data: ");
			if (sdata_p==NULL) {
				freeMem = 1;
				sdata_p = malloc(sizeof(SensorData));
			}
			memset(sdata_p, 255, sizeof(SensorData));
			sdata_p->scale = DIS_DATA_SCALE_NONE;
			while (offset+2<len) {
				offset += decodeTlv(numDataStore, ds, macAddr, macAddrStr, data_p+2+offset, sdata_p);
			}
			sensorVal = getValue(sdata_p->value_p, sdata_p->valLen);
			if (sdata_p->type==PLTFRM_ON_CHIP_VCC_SENSOR_DEV_ID) {
				onChipVccValue = sensorVal; // TODO Process scale as well: now assumed as mV
				memcpy(onChipVccMacAddr, macAddr, sizeof(onChipVccMacAddr)); // Vcc saved for this particular node
			}
			else if (memcmp(onChipVccMacAddr, macAddr, sizeof(onChipVccMacAddr))) {
				onChipVccValue = 3150; // use this as default
			}
			UINT32_t convertedVal = convertSensorValue(sdata_p->type, sensorVal, onChipVccValue);
			if (convertedVal!=sensorVal)
				printf("%s %s %lu->%lu\n", getDevName(sdata_p->type), getScale(sdata_p->scale), sensorVal, convertedVal);
			else
				printf("%s %s %lu\n", getDevName(sdata_p->type), getScale(sdata_p->scale), sensorVal);

			SensorInfo *sensorInfo = getSensorInfo(sdata_p->type);
			if (sensorInfo==NULL) break;

#ifdef API_ENA
			// This is an optimization: save all sensor values and make a single API call
			ds[*numDataStore].type = sdata_p->type;
			ds[*numDataStore].convertedValue = convertedVal;
			(*numDataStore)++;

			// Invoke notifications right away on gateway: relying on API_CACHE_ENA for performance
			String *result = readRule(mainCon, macAddr);
			if (result) {
				json_error_t error;
				json_t *root = json_loads(result->ptr, 0, &error);
				unsigned int i, j;
				unsigned char numSensors = json_array_size(root);
				for(i=0; i < numSensors; i++) {
					json_t *sensor = json_array_get(root, i);
					if (atoi(json_string_value(json_object_get(sensor, "sensorId")))!=sdata_p->type) continue;

					json_t *rules = json_object_get(sensor, "rules");
					unsigned char numRules = json_array_size(rules);
					for(j=0; j < numRules; j++) {
						json_t *rule = json_array_get(rules, j);
						unsigned int notifyPeriod = atoi(json_string_value(json_object_get(rule, "notifyPeriod")));
						unsigned int elapsedSecs = atoi(json_string_value(json_object_get(rule, "elapsedSecs")));
						if (elapsedSecs > notifyPeriod &&
							strcmp("Gateway", json_string_value(json_object_get(rule, "actionOn")))==0) {
							ruleNotify(macAddr, macAddrStr, sdata_p, convertedVal,
								atoi(json_string_value(json_object_get(rule, "ruleUid"))),
								json_string_value(json_object_get(rule, "type")),
								json_string_value(json_object_get(rule, "threshold")),
								json_string_value(json_object_get(rule, "actionOn")),
								json_string_value(json_object_get(rule, "mobileNum")));
						}
					}
				}
				json_decref(root);
				free(result->ptr);
				free(result);
			}
#else
			updateSensorInfo(mainCon, macAddr, sensorInfo);
			if (updateSensorData(mainCon, macAddr, sdata_p->type, convertedVal)) {
				offset += 2;
				break;
			}

			MYSQL_RES *result = readRule(mainCon, macAddr, sdata_p->type);
			if (result) {
				MYSQL_ROW row = mysql_fetch_row(result);
				while (row && atoi(row[9])>atoi(row[6])) {
					ruleNotify(macAddr, macAddrStr, sdata_p, convertedVal, atoi(row[0]), row[3], row[4], row[5], row[8]);
					row = mysql_fetch_row(result);
				}
				mysql_free_result(result);
			}
#endif

			offset += 2;
			break;
		case DIS_TLV_TYPE_SENSOR_ID:
			sdata_p->type = data_p[2];
			offset = len+2;
			break;
		case DIS_TLV_TYPE_VALUE:
			sdata_p->value_p = data_p+2;
			sdata_p->valLen = len;
			offset = len+2;
			break;
		case DIS_TLV_TYPE_DATA_SCALE_FACTOR:
			sdata_p->scale = data_p[2];
			offset = len+2;
			break;
		default:
			offset = 255;
			break;
	}

	if (freeMem) free(sdata_p);

	return offset;
}

UINT8_t processRelayPkt(UINT8_t *rxPkt)
{
#ifndef SNIFFER
	rxPkt += UART_FRAME_HDR_LEN;

	UINT16_t macShortAddr = myntohs(rxPkt);
	rxPkt += 2;

	UINT8_t macAddr[MAC_EXT_ADDR_LEN];
	memcpy(macAddr, rxPkt, sizeof(macAddr));
	rxPkt += MAC_EXT_ADDR_LEN;

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	SINT8_t rssi=rxPkt[0];
	UINT8_t corr=rxPkt[1];
#ifndef API_ENA
	updateRssiAndCorrelation(mainCon, macAddr, rssi, corr);
#endif
	printf("MacAddr: 0x%s, ShortAddr: 0x%04X\n", macAddrStr, macShortAddr);
	printf("Link Data: RSSI %hhddBm, LQI %d\n", (char)rxPkt[0], rxPkt[1]);
	rxPkt += 2;

	// TODO Filter by supported message types before decoding TLV
	UINT8_t numDataStore = 0;
	SensorDataStore dataStore[8]; // TODO Increase number of sensors updated together
	decodeTlv(&numDataStore, dataStore, macAddr, macAddrStr, rxPkt+1, NULL);

#ifdef API_ENA
	updateAllSensorData(mainCon, macAddr, rssi, corr, numDataStore, dataStore);
#endif

#ifdef API_ENA
	// This is like polling but we do it when we have just sent sensor data
	PendingAction *actions = NULL;
	unsigned char numActions = 0;
	getAllPendingActions(mainCon, &numActions, &actions);
	if (numActions && actions) {
		unsigned int i;
		for (i=0; i<numActions; i++) {
			processAction(actions+i);
		}
		free(actions);
	}
#endif

	free(macAddrStr);
#else
	UINT16_t rxPyldSize = myntohs(rxPkt+UART_FRAME_HDR_PYLD_LEN_FIELD_OFF);
	UINT8_t verbose = 1;
	time_t frameRxTime;
	time(&frameRxTime);

	rxPkt += UART_FRAME_HDR_LEN + MAC_EXT_ADDR_LEN;
    if (rxPyldSize >= LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN + LPWMN_MSG_RSSI_LEN + LPWMN_MSG_CORR_LQI_LEN)
    {
        unsigned short frameIdx = myntohs(rxPkt);
        int rssi = *((char *)(rxPkt + LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN));
        int lqi = *((char *)(rxPkt + LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN + LPWMN_MSG_RSSI_LEN));
        char *timeStr_p = ctime(&frameRxTime);

        timeStr_p[strlen(timeStr_p) - 1] = '\0';

        if (verbose)
            printf("<%s> frame Index<%04u> \n", __FUNCTION__, frameIdx);

        printf("#<%04u> <%s> rssi<%d dBm> lqi<%d> len<%d>\n",
               frameIdx, timeStr_p, rssi, lqi, rxPyldSize);

        rxPkt += (LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN
                   + LPWMN_MSG_RSSI_LEN
                   + LPWMN_MSG_CORR_LQI_LEN);

        rxPyldSize -= (LPWMN_SNIFFER_FRAME_IDX_FIELD_LEN
                    + LPWMN_MSG_RSSI_LEN
                    + LPWMN_MSG_CORR_LQI_LEN);

        if (verbose)
            printf("<%s> rxPyldSize left<0x%x> \n", __FUNCTION__, rxPyldSize);

        if (rxPyldSize > 0)
        {
            int idx;
            // Decode the PDU
            __decodePdu(rxPkt, rxPyldSize);

            // TODO Printing of payload is skipped for now
            for (idx=0; 0 && idx<rxPyldSize; idx++)
            {
                 if (idx % 8 == 0)
                     printf("\n[%03u] : ", idx);
                 printf("0x%02x ", rxPkt[idx]);
            }
        }
        printf("\n--------------------------------------------------------------------------\n\n\n");
    }
#endif

	return 0;
}

UINT8_t processEvent(UINT8_t *rxPkt)
{
	char eventMsg[128+1];

	if (!isExpectedPyldLen(rxPkt)) return 1;

	// Header no longer needed in this function
	rxPkt += UART_FRAME_HDR_LEN;

	char *coordMacAddrStr = NULL;
	UINT8_t *coordMacAddr = NULL;

	NodeInfo nodeInfo;
	char *macAddrStr;

	UINT8_t eventType = *rxPkt;
	switch (eventType)
	{
		case LPWMN_GW_EVT_TYPE_NODE_REG:
			nodeInfo.macShortAddr = myntohs(rxPkt+1);
			memcpy(nodeInfo.macAddr, rxPkt+1+2, sizeof(nodeInfo.macAddr));
			nodeInfo.macCapability = *(rxPkt+1+2+8);
			nodeInfo.pushPeriod = NODE_DEFAULT_PUSH_PERIOD_SEC;
			updateNodeInfo(mainCon, &nodeInfo);
			updateNodeSensors(mainCon, nodeInfo.macAddr, fixedSensors);
			updateRouteInfo(mainCon, nodeInfo.macAddr); // TODO Use LPWMN_GW_MSG_TYPE_PATH_DISC_REQ and process response
			sleep(1); // TODO Remove this delay after node registration
#ifdef API_ENA
			setNodePushPeriod(nodeInfo.macShortAddr, readPushPeriod(mainCon, nodeInfo.macAddr, 1));
#else
			setNodePushPeriod(nodeInfo.macShortAddr, readPushPeriod(mainCon, nodeInfo.macAddr));
#endif

			macAddrStr = getStringForm(nodeInfo.macAddr, MAC_EXT_ADDR_LEN);
			snprintf(eventMsg, sizeof(eventMsg), "MacAddr=0x%s, ShortAddr=0x%04X, MacCapability=0x%02X",
					macAddrStr, nodeInfo.macShortAddr, nodeInfo.macCapability);
			free(macAddrStr);
			insertEvent(mainCon, nodeInfo.macAddr, "Info", "NodeAssoc", eventMsg);
			getFatalErrId(nodeInfo.macShortAddr);
			scheduleHdr(LPWMN_GW_MSG_TYPE_NODE_CNT_REQ);
			break;
		case LPWMN_GW_EVT_TYPE_LOCAL_RT_DISC_STARTED:
			// TODO Process LPWMN_GW_EVT_TYPE_LOCAL_RT_DISC_STARTED event
			break;
		case LPWMN_GW_EVT_TYPE_SYS_BOOT:
 			coordMacAddrStr = readCoordExtAddr(mainCon);
			coordMacAddr = parseString(coordMacAddrStr);
 			insertEvent(mainCon, coordMacAddr, "Info", "Boot", "Coordinator has rebooted.");
			free(coordMacAddrStr);
			free(coordMacAddr);
			startCoord();
			scheduleNwInfo();
			break;
	}

	return 0;
}

UINT8_t getNodeWhiteList()
{
	// TODO Delete this unused function: list will be read from DB and set on the GW
	UINT16_t i = 0;
	UINT8_t nodeIndex[2], macAddr[8], invalidMac[8];
	UINT8_t *rxPkt;
	UINT8_t retVal = 0;

	memset(invalidMac, 0xFF, sizeof(invalidMac));
	myhtons(nodeIndex, i);
	rxPkt = txPktRxPkt(LPWMN_GW_MSG_TYPE_GET_WHITE_LIST_ENTRY, nodeIndex, 2, 3);
	while (rxPkt) {
		retVal |= processPkt(macAddr, rxPkt);
		free(rxPkt);
		if (retVal || memcmp(invalidMac,macAddr,sizeof(macAddr))==0)
			break;
		addToNodeWhiteList(mainCon, macAddr);
		myhtons(nodeIndex, ++i);
		rxPkt = txPktRxPkt(LPWMN_GW_MSG_TYPE_GET_WHITE_LIST_ENTRY, nodeIndex, 2, 3);
	}

	return retVal;
}

UINT8_t getFatalErrId(UINT16_t macShortAddr)
{
	UINT8_t shortAddr[2];
	myhtons(shortAddr, macShortAddr);

	UINT8_t *errCode = txPktRxPkt(LPWMN_GW_MSG_TYPE_GET_NODE_LAST_FATAL_ERR_ID, shortAddr, 2, 3);
	if (errCode) {
		UINT16_t errCodeVal;
		errCodeVal = myntohs(errCode+UART_FRAME_HDR_LEN);
		free(errCode);
		if (errCodeVal) {
			char printBuff[32];
			snprintf(printBuff, sizeof(printBuff), "Fatal error ID is %u.", errCodeVal);
			char *macAddrStr = readNodeExtAddr(mainCon, macShortAddr);
			if (macAddrStr) {
				UINT8_t *macAddr = parseString(macAddrStr);
				return insertEvent(mainCon, macAddr, "Fatal", "FatalErrId", printBuff);
				free(macAddrStr);
				free(macAddr);
			}
		}
		return 0;
	}

	return 1;
}

UINT8_t startCoord(void)
{
	UINT8_t channelNum[2];
	UINT8_t *rxPkt;
	UINT16_t currChannel, savedChannel;

	rxPkt = txHdrRxPkt(LPWMN_GW_MSG_TYPE_GET_NWK_LPWMN_ID, 3);
	if (rxPkt) {
		processPkt(NULL, rxPkt);
		free(rxPkt);
	}
	else return 1;

	getFatalErrId(PAN_COORD_SHORT_ADDR);

	rxPkt = txHdrRxPkt(LPWMN_GW_MSG_TYPE_GET_NWK_CHANNEL, 3);
	if (rxPkt) {
		currChannel = myntohs(rxPkt+UART_FRAME_HDR_LEN);
		free(rxPkt);
	}
	else return 1;

	savedChannel = readChannelNumber(mainCon);
	if (savedChannel==0) savedChannel = DEFAULT_CHANNEL_NUM;
	myhtons(channelNum, savedChannel);
	rxPkt = txPktRxPkt(LPWMN_GW_MSG_TYPE_SET_RADIO_CHANNEL, channelNum, 2, 3);
	if (rxPkt && rxPkt[UART_FRAME_HDR_LEN]==0) { // Coord already started
		free(rxPkt);
		if (savedChannel!=currChannel) {
			rxPkt = txHdrRxPkt(LPWMN_GW_MSG_TYPE_REBOOT_COORD, 3);
#ifndef API_ENA
			clearAllPendingActions(mainCon);
#endif
			free(rxPkt);
			return 1;
		}
		return 0;
	}
	else free(rxPkt);

	UINT8_t nodeAssocState = readNodeAssocState(mainCon);
	if (nodeAssocState)
		rxPkt = txHdrRxPkt(LPWMN_GW_MSG_TYPE_DISABLE_NWK_JOINS, 3);
	else
		rxPkt = txHdrRxPkt(LPWMN_GW_MSG_TYPE_ENABLE_NWK_JOINS, 3);
	free(rxPkt);

	setNodeWhiteList();

	setTimeout(10, 0);
	rxPkt = txHdrRxPkt(LPWMN_GW_MSG_TYPE_START_COORD, 3);
	free(rxPkt);
	setDefaultTimeout();

	return 0;
}

UINT8_t getNodeInfo(NodeInfo *nodeInfo_p, UINT16_t i)
{
	UINT8_t *rxPkt = NULL;
	UINT8_t txPayload[2];
	int returnValue = 0;

	memset(nodeInfo_p, 0, sizeof(NodeInfo));

	myhtons(txPayload, i);
	rxPkt = txPktRxPkt(LPWMN_GW_MSG_TYPE_NODE_LIST_REQ, txPayload, 2, 3);
	if (rxPkt) {
		UINT16_t rxPyldSize = myntohs(rxPkt+UART_FRAME_HDR_PYLD_LEN_FIELD_OFF);
		if (rxPyldSize==2) { // invalid node index
			returnValue = 0xFF;
		}
		else {
			UINT16_t rcvdIndex = myntohs(rxPkt+UART_FRAME_HDR_LEN);
			if (rcvdIndex == i)
			{
				nodeInfo_p->macShortAddr = myntohs(rxPkt+UART_FRAME_HDR_LEN+2);
				nodeInfo_p->parentMacShortAddr = myntohs(rxPkt+UART_FRAME_HDR_LEN+2+2);
				memcpy(nodeInfo_p->macAddr, rxPkt+UART_FRAME_HDR_LEN+2+2+2, sizeof(nodeInfo_p->macAddr));
				nodeInfo_p->macCapability = *(rxPkt+UART_FRAME_HDR_LEN+2+2+2+8);
				nodeInfo_p->timeSinceLastRx = myntohs(rxPkt+UART_FRAME_HDR_LEN+2+2+2+8+1);
				nodeInfo_p->pushPeriod = NODE_DEFAULT_PUSH_PERIOD_SEC;
			}
			else returnValue = 1;
		}
		free(rxPkt);
	}
	else returnValue = 1;

	return returnValue;
}

void initAllNodeInfo(void)
{
	UINT8_t nodeIndex=0, retVal;
	NodeInfo nodeInfo;
	retVal = getNodeInfo(&nodeInfo, nodeIndex);
	while (retVal!=0xFF) {
		if (retVal) {
			LOG(LOG_WARN, "Error in getting nodeInfo info [%d].", nodeIndex);
			return;
		}
		updateNodeInfo(mainCon, &nodeInfo);
		updateNodeSensors(mainCon, nodeInfo.macAddr, fixedSensors);
		updateRouteInfo(mainCon, nodeInfo.macAddr); // TODO Use LPWMN_GW_MSG_TYPE_PATH_DISC_REQ and process response
		sleep(1); // TODO Remove this delay during boot up
#ifdef API_ENA
		setNodePushPeriod(nodeInfo.macShortAddr, readPushPeriod(mainCon, nodeInfo.macAddr, 1));
#else
		setNodePushPeriod(nodeInfo.macShortAddr, readPushPeriod(mainCon, nodeInfo.macAddr));
#endif
		retVal = getNodeInfo(&nodeInfo, ++nodeIndex);
	}
}

void digitalIOCtrl(UINT16_t shortAddr, unsigned int portId, unsigned int pinNr, unsigned int val)
{
	unsigned int pyldLen, off = 0;
	unsigned char *pyld_p;

	pyldLen = MAC_SHORT_ADDR_LEN
				+ DIS_MSG_TYPE_SZ
				+ DIS_TLV_HDR_SZ
				+ DIS_GPIO_PORT_ID_TLV_SZ
				+ DIS_GPIO_PIN_ID_TLV_SZ
				+ DIS_GPIO_VAL_TLV_SZ;

	pyld_p = (unsigned char *)malloc(pyldLen);

	myhtons(pyld_p, shortAddr);
	off = MAC_SHORT_ADDR_LEN;

	pyld_p[off ++] = DIS_MSG_TYPE_CTRL_DIGITAL_IO;

	pyld_p[off ++] = DIS_TLV_TYPE_GPIO_CTRL;
	pyld_p[off ++] = (DIS_GPIO_PORT_ID_TLV_SZ
						+ DIS_GPIO_PIN_ID_TLV_SZ
						+ DIS_GPIO_VAL_TLV_SZ);

	pyld_p[off ++] = DIS_TLV_TYPE_GPIO_PORT_ID;
	pyld_p[off ++] = DIS_GPIO_PORT_ID_FIELD_SZ;
	pyld_p[off ++] = portId;

	pyld_p[off ++] = DIS_TLV_TYPE_GPIO_PIN_ID;
	pyld_p[off ++] = DIS_GPIO_PIN_ID_FIELD_SZ;
	pyld_p[off ++] = pinNr;

	pyld_p[off ++] = DIS_TLV_TYPE_GPIO_VAL;
	pyld_p[off ++] = DIS_GPIO_VAL_FIELD_SZ;
	pyld_p[off ++] = val;

	UINT8_t *rxPkt = txPktRxPkt(LPWMN_GW_MSG_TYPE_RELAY_TO_NODE, pyld_p, pyldLen, 3);
	free(rxPkt);
}

void setNodeWhiteList(void)
{
#ifdef API_ENA
	char *whiteList = (void*)readNodeWhiteList(mainCon);
	if (whiteList) {
		unsigned char numNodes = *whiteList;
		unsigned int i=0;
		for (i=0; i<numNodes; i++) {
			UINT8_t *rxPkt = txPktRxPkt(LPWMN_GW_MSG_TYPE_ADD_NODE_TO_WHITE_LIST, (UINT8_t*)(whiteList+1+i*8), 8, 3);
			free(rxPkt);
		}
		free(whiteList);
	}
#else
	MYSQL_RES *result = readNodeWhiteList(mainCon);
	if (result) {
		MYSQL_ROW row = mysql_fetch_row(result);
		while (row) {
			char macAddrStr[17];
			sprintf(macAddrStr, "%s", row[0]);
			macAddrStr[16] = 0;
			UINT8_t *macAddr = parseString(macAddrStr);
			UINT8_t *rxPkt = txPktRxPkt(LPWMN_GW_MSG_TYPE_ADD_NODE_TO_WHITE_LIST, macAddr, 8, 3);
			free(rxPkt);
			free(macAddr);
			row = mysql_fetch_row(result);
		}
		mysql_free_result(result);
	}
#endif
}

void setNodePushPeriod(UINT16_t macShortAddr, UINT16_t pushPeriod)
{
	UINT16_t pyldLen = 0;
	UINT8_t *payLoad;
	pyldLen = MAC_SHORT_ADDR_LEN
			+ DIS_MSG_TYPE_SZ
			+ DIS_TLV_HDR_SZ
			+ LPWMN_GW_MSG_NODE_DATA_PUSH_INTERVAL_FIELD_LEN;
	payLoad = malloc(pyldLen);

	myhtons(payLoad, macShortAddr);

	UINT16_t off = MAC_SHORT_ADDR_LEN;
	payLoad[off++] = DIS_MSG_TYPE_CFG_NODE_DATA_PUSH_INTERVAL;
	payLoad[off++] = DIS_TLV_TYPE_PUSH_INTERVAL;
	payLoad[off++] = LPWMN_GW_MSG_NODE_DATA_PUSH_INTERVAL_FIELD_LEN;
	myhtons(payLoad+off, pushPeriod);

	schedulePkt(LPWMN_GW_MSG_TYPE_RELAY_TO_NODE, payLoad, pyldLen);

	free(payLoad);
}
