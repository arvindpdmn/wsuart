#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <strings.h>
#include <string.h>
#include <wsuart.h>

char GSM_modemSerDevName[] = "/dev/ttyUSB1";

#define SMS_PYTHON_CODE // TODO Delete Python implementation once C code is working
#ifdef SMS_PYTHON_CODE

int GSM_initModem(char *modemPortStr_p)
{
	// Nothing to do here: init is done just before SMS is sent
	return 0;
}

int GSM_sendSMS(char *msg, const char *mobileNum)
{
	int retVal = 0;

	char buff[256];
	sprintf(buff, "python ../src/sms.py %s '%s'", mobileNum, msg);
	FILE *fp = popen(buff, "r");
	while (fgets(buff, sizeof(buff), fp) != NULL); // ignore what you read
	pclose(fp);

	return retVal;
}

#else

#define SER_BUFF_LEN  256

char GSM_serTxBuff[SER_BUFF_LEN];
char GSM_serRxBuff[SER_BUFF_LEN + 1];

static cntxt_s GSM_uartCntxt;


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int GSM_writePort(char *buff_p, unsigned int cnt)
{
   int rc, bytesLeft = cnt, bytesWritten = 0;

   // printf("\n<%s> cnt<%d> \n", __FUNCTION__, cnt);

   while (bytesLeft > 0)
   {
      rc = write(GSM_uartCntxt.serialFd, buff_p + bytesWritten, bytesLeft);
      if (rc <= 0)
          return -1;
      else
      {
          bytesLeft -= rc;
          bytesWritten += rc;
      }
   }

   return 1;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int readEcho(unsigned char *buff_p, int buffLen)
{
   int rdLen, readLeft = buffLen, totRead = 0;

   while (readLeft > 0)
   {
      rdLen = read(GSM_uartCntxt.serialFd, buff_p + totRead, 1);
      if (rdLen > 0)
      { 
          // printf("\n Read <%c / %x> \n", buff_p[totRead], buff_p[totRead]);
          totRead += rdLen;
          readLeft -= rdLen;
          // if (buff_p[totRead] == '\n')
          //     break;
      }
      else
      {
          printf("\n<%s> read() failed  - %d !! \n", __FUNCTION__, rdLen);
          return rdLen;
      }
   }

   return totRead;
}



/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int readByte(char *buff_p)
{
   int rdLen;
   // printf("\n Waiting for response ..... \n");
   rdLen = read(GSM_uartCntxt.serialFd, buff_p, 1);
   return rdLen == 1 ? 1 : -1;
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int readByteSeq(char *buff_p, int byteCnt)
{
   int rdLen, readLeft = byteCnt, totRead = 0;

   while (readLeft > 0)
   {
      rdLen = read(GSM_uartCntxt.serialFd, buff_p + totRead, 1);
      if (rdLen > 0)
      { 
          // printf("\n Read <%c / %x> \n", buff_p[totRead], buff_p[totRead]);
          totRead += rdLen;
          readLeft -= rdLen;
      }
      else
      {
          if (errno != EAGAIN)
          {
              printf("\n<%s> read() failed  - %d !! \n", __FUNCTION__, rdLen);
              return -1;
          }
      }
   }

   return totRead;
}



/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int readResp(unsigned char *buff_p, int buffLen)
{
   int rdLen, readLeft = 2, totRead = 0;

   rdLen = read(GSM_uartCntxt.serialFd, buff_p, 2);
   if (rdLen > 0)
   {
       if (buff_p[0] != 0xd || buff_p[1] != 0xa)
           return -1;
   }

   readLeft = buffLen;

   while (readLeft > 0)
   {
      rdLen = read(GSM_uartCntxt.serialFd, buff_p + totRead, 1);
      if (rdLen > 0)
      {
          // printf("\n Read <%c / %x> \n", buff_p[totRead], buff_p[totRead]);
          totRead += rdLen;
          readLeft -= rdLen;

          if (totRead >= 2)
          {
              if (buff_p[totRead-2] == 0xd && buff_p[totRead-1] == 0xa)
              {
                  buff_p[totRead-2] = 0x0;
                  return totRead - 2;
              }

          }
      }

   }

   return -2;
}
    


#ifdef __CYGWIN__

/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int GSM_cfgPort(char *serDevName_p, int baudRate)
{
    struct termios newtio;
    struct termios oldtio;
    struct termios latesttio;
    cntxt_s *serialCntxt_p = &GSM_uartCntxt;
    int rc;

    serialCntxt_p->serialFd = open(serDevName_p, O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (serialCntxt_p->serialFd < 0)
    {
        printf("Failed to open serial device <%s> - errno<%d> !!\n",
               serDevName_p, errno);  
        return -1;
    }

    rc = tcgetattr(serialCntxt_p->serialFd, &oldtio); /* save current port settings */
    if (rc < 0)
    {
        perror("error ... \n");
        printf("\n tcgetattr() failed !! - rc<%d>, errno<%d> \n", rc, errno);
        return -1;
    }

    bzero(&newtio, sizeof(newtio));

    rc = cfsetspeed(&newtio, baudRate);
    if (rc < 0)
    {
        printf("\n cfsetspeed() failed !! - rc<%d>, errno<%d> \n", rc, errno);
        return -1;
    }

    newtio.c_cflag = CS8 | CLOCAL | CREAD;
    newtio.c_iflag = IGNPAR;
    newtio.c_oflag = 0;

    // if ((rc = fcntl(serialCntxt_p->serialFd, F_SETOWN, getpid())) < 0)
    // {
    //     printf("\n fcntl failed !! - rc<%d>, errno<%d> \n", rc, errno);
    //     return -1;
    // }

    /* set input mode (non-canonical, no echo,...) */
    newtio.c_lflag = 0;

    newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
    newtio.c_cc[VMIN]     = 1;   /* blocking read until 10 chars received */

    rc = tcsetattr(serialCntxt_p->serialFd, TCSANOW, &newtio);
    if (rc < 0)
    {
        printf("\n tcsetattr() failed !! - rc<%d> / errno<%d> \n", rc, errno);
        return -1;
    }

    rc = tcflush(serialCntxt_p->serialFd, TCIFLUSH);
    if (rc < 0)
    {
        printf("\n tcflush() failed !! - rc<%d> \n", rc);
        return -1;
    }
    
    tcgetattr(serialCntxt_p->serialFd, &latesttio); 
    if (rc < 0)
    {
        printf("\n tcgetattr() failed !! - rc<%d> \n", rc);
        return -1;
    }

    // printf("\nispeed<%d> / ospeed<%d> \n", latesttio.c_ispeed, latesttio.c_ospeed);
    // printf("\niflag<0x%x>/oflag<0x%x>/cflag<0x%x> \n", latesttio.c_iflag, latesttio.c_oflag, latesttio.c_cflag);

    return 1;
}



#else 
                             
/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
int GSM_cfgPort(char *serDevName_p, int baudRate)
{
   cntxt_s *serialCntxt_p = &GSM_uartCntxt;

   memset(serialCntxt_p, 0, sizeof(cntxt_s));

   serialCntxt_p->serialFd = open((char *)serDevName_p, O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
   if (serialCntxt_p->serialFd < 0)
   {
       printf("\n<%s> open(%s) failed !! - errno<%d> \n",
              __FUNCTION__, serDevName_p, errno);
       return -1;
   }
   
   // Zero out port status flags
   if (fcntl(serialCntxt_p->serialFd, F_SETFL, 0) != 0x0)
   {
       return -1;
   }

   bzero(&(serialCntxt_p->dcb), sizeof(serialCntxt_p->dcb));

   // serialCntxt_p->dcb.c_cflag |= serialCntxt_p->baudRate;  // Set baud rate first time
   serialCntxt_p->dcb.c_cflag |= baudRate;  // Set baud rate first time
   serialCntxt_p->dcb.c_cflag |= CLOCAL;  // local - don't change owner of port
   serialCntxt_p->dcb.c_cflag |= CREAD;  // enable receiver

   // Set to 8N1
   serialCntxt_p->dcb.c_cflag &= ~PARENB;  // no parity bit
   serialCntxt_p->dcb.c_cflag &= ~CSTOPB;  // 1 stop bit
   serialCntxt_p->dcb.c_cflag &= ~CSIZE;  // mask character size bits
   serialCntxt_p->dcb.c_cflag |= CS8;  // 8 data bits

   // Set output mode to 0
   serialCntxt_p->dcb.c_oflag = 0;
 
   serialCntxt_p->dcb.c_lflag &= ~ICANON;  // disable canonical mode
   serialCntxt_p->dcb.c_lflag &= ~ECHO;  // disable echoing of input characters
   serialCntxt_p->dcb.c_lflag &= ~ECHOE;
 
   // Set baud rate
   cfsetispeed(&serialCntxt_p->dcb, serialCntxt_p->baudRate);
   cfsetospeed(&serialCntxt_p->dcb, serialCntxt_p->baudRate);

   serialCntxt_p->dcb.c_cc[VTIME] = 0;  // timeout = 0.1 sec
   serialCntxt_p->dcb.c_cc[VMIN] = 0;
 
   if ((tcsetattr(serialCntxt_p->serialFd, TCSANOW, &(serialCntxt_p->dcb))) != 0)
   {
       printf("\ntcsetattr(%s) failed !! - errno<%d> \n",
              serDevName_p, errno);
       close(serialCntxt_p->serialFd);
       return -1;
   }

   // flush received data
   tcflush(serialCntxt_p->serialFd, TCIFLUSH);
   tcflush(serialCntxt_p->serialFd, TCOFLUSH);

   return 1;
}

#endif

void __flushRx(void)
{
   do
   {
       int rc;
       char c;
       rc = readByte(&c);
       if (rc != 1)
           break;
   } while (1);
}
   

int __getResp(char *buff_p, int buffLen)
{
   int rc, off = 0;
   do
   {
       rc = readByte(buff_p + off);
       if (rc != 1)
       {
           if (errno != EAGAIN)
               return -1;
       }
       else
       {
           // printf("\n<%s> rd<0x%x>/<%c> \n", __FUNCTION__, buff_p[off], buff_p[off]);
           off ++;
           if (off > 2)
           {
               if (GSM_serRxBuff[off-2] == 13 && GSM_serRxBuff[off-1] == 10)
               {
                   
                   rc = 1;
                   break;
               }
               else
               {
                   if (off >= buffLen)
                   {
                       rc = -2;
                       break;
                   }
               }
           }
       }
   } while (1);

   if (rc == 1)
   {
       GSM_serRxBuff[off-2] = '\0';
   }

   return rc;
}

#define MODEM_MFR_ID "Telit"
#define SMS_TX_CMD 0x1a 

char resultBuff[256];

/*
 *****************************************************************
 *
 *
 *
 *
 *****************************************************************
 */
int GSM_initModem(char *modemPortStr_p)
{
   int rc, cmdLen;

   printf("\nConfiguring serial port <%s> \n", modemPortStr_p);

   if (GSM_cfgPort(modemPortStr_p, B115200) < 0)
       return 2;

   // Disable echo
   
   printf("Disabling echo ...");

   cmdLen = sprintf(GSM_serTxBuff, "ATE0");
   GSM_serTxBuff[cmdLen++] = 13;
   GSM_serTxBuff[cmdLen++] = 10;
   
   rc = GSM_writePort(GSM_serTxBuff, cmdLen);
   if (rc != 1)
   {
       printf("\nGSM_writePort() failed !!\n");
       return 3;
   }

   sleep(1);

   __flushRx();

   printf(" done \n");
   
   printf("Getting Mfr Info ...");

   // Get the manufacturer id as a test
   cmdLen = sprintf(GSM_serTxBuff, "AT+GMI");
   GSM_serTxBuff[cmdLen ++] = 13;
   GSM_serTxBuff[cmdLen ++] = 10;
   rc = GSM_writePort(GSM_serTxBuff, cmdLen);
   if (rc != 1)
   {
       printf("\nGSM_writePort() failed !!\n");
       return 3;
   }

   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
   {
       printf("\n error 11 .... \n");
       return 4;
   }

   // printf("\n resp rcvd - <%s> \n", GSM_serRxBuff);
   if (GSM_serRxBuff[0] != 13 || GSM_serRxBuff[1] != 10)
   {
       printf("\n error 12 .... \n");
       return 5;
   }

   printf(" %s\n", GSM_serRxBuff + 2);
   if (strcmp(GSM_serRxBuff + 2, MODEM_MFR_ID) != 0x0)
   {
       printf("\n error 13 .... \n");
       return 6;
   }

   // Set the SMS mode to text
   // 0 - PDU mode, as defined in GSM 3.40 and GSM 3.41 (factory default)
   // 1 - text mode
   cmdLen = sprintf(GSM_serTxBuff, "AT+CMGF=1");
   GSM_serTxBuff[cmdLen ++] = 13;
   GSM_serTxBuff[cmdLen ++] = 10;
   rc = GSM_writePort(GSM_serTxBuff, cmdLen);
   if (rc != 1)
   {
       printf("\n error 14 .... \n");
       return 7;
   }

   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
   {
       printf("\n error 15 .... \n");
       return 8;
   }

   // printf("\n resp rcvd - <%s> \n", GSM_serRxBuff);
   if (GSM_serRxBuff[0] != 13 || GSM_serRxBuff[1] != 10)
   {
       printf("\n error 16 .... \n");
       return 9;
   }

   if (strcmp(GSM_serRxBuff + 2, "OK") != 0x0)
   {
       printf("\n error 17 .... \n");
       return 10;
   }


   // Check SIM status
   printf("Checking SIM status ... ");

   cmdLen = sprintf(GSM_serTxBuff, "AT+CPIN?");
   GSM_serTxBuff[cmdLen ++] = 13;
   GSM_serTxBuff[cmdLen ++] = 10;
   rc = GSM_writePort(GSM_serTxBuff, cmdLen);
   if (rc != 1)
       return 11;

   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 12;

   // printf("\n resp rcvd - <%s> \n", GSM_serRxBuff);
   if (GSM_serRxBuff[0] != 13 || GSM_serRxBuff[1] != 10)
       return 13;
   
   if (strcmp(GSM_serRxBuff + 2, "OK") != 0x0)
   {
       printf(" %s !!! \n", GSM_serRxBuff + 2);
       return 14;
   }

   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 15;
   
   // printf("\n resp rcvd - <%s> \n", GSM_serRxBuff + 2);
   if (strcmp(GSM_serRxBuff + 2, "+CPIN: READY") != 0x0)
       return 16;

   printf(" Good (%s) \n", GSM_serRxBuff + 2);

   // -----------------------------------------------------------------------

   // Check SIM status
   printf("Checking network registration status ... ");

   // Get network registration status
   cmdLen = sprintf(GSM_serTxBuff, "AT+CREG?");
   GSM_serTxBuff[cmdLen ++] = 13;
   GSM_serTxBuff[cmdLen ++] = 10;
   rc = GSM_writePort(GSM_serTxBuff, cmdLen);
   if (rc != 1)
       return 17;
   
   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 18;

   if (GSM_serRxBuff[0] != 13 || GSM_serRxBuff[1] != 10)
   {
       printf(" Bad response !! \n");
       return 19;
   }

   // printf("\n resp rcvd - <%s> \n", GSM_serRxBuff + 2);
   
   if (strcmp(GSM_serRxBuff + 2, "OK") != 0x0)
   {
       printf(" Failed !! \n");
       return 20;
   }

   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 21;
   
   if (GSM_serRxBuff[0] != 13 || GSM_serRxBuff[1] != 10)
       return 22;
   // printf("\n resp rcvd - <%s> \n", GSM_serRxBuff + 2);
   
   if (strcmp(GSM_serRxBuff + 2, "+CREG: 0,1") != 0x0)
       return 23;

   printf("Registered (%s) \n", GSM_serRxBuff + 2);

   // -----------------------------------------------------------------------

   // Get provider 
   printf("Getting provider info ... ");

   cmdLen = sprintf(GSM_serTxBuff, "AT+COPS?");
   GSM_serTxBuff[cmdLen ++] = 13;
   GSM_serTxBuff[cmdLen ++] = 10;
   rc = GSM_writePort(GSM_serTxBuff, cmdLen);
   if (rc != 1)
       return 24;
   
   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 25;

   if (GSM_serRxBuff[0] != 13 || GSM_serRxBuff[1] != 10)
       return 26;
   // printf("\n resp rcvd - <%s> \n", GSM_serRxBuff + 2);
   
   if (strcmp(GSM_serRxBuff + 2, "OK") != 0x0)
       return 27;

   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 28;
   
   if (GSM_serRxBuff[0] != 13 || GSM_serRxBuff[1] != 10)
       return 29;

   // printf("\n resp rcvd - <%s> \n", GSM_serRxBuff + 2);

   printf(" %s \n",  GSM_serRxBuff + 2);
   
   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 30;
   
   // printf(" %s \n",  GSM_serRxBuff + 2);
  
   return 0;
}



int GSM_sendSMS(char *msg, char *mobileNum)
{
   int rc, cmdLen, refNr;

   printf("\n Configuring phone number <%s> ... ",  mobileNum);

   cmdLen = sprintf(GSM_serTxBuff, "AT+CMGS=\"+91%s\"", mobileNum);
   GSM_serTxBuff[cmdLen ++] = 13;
   rc = GSM_writePort(GSM_serTxBuff, cmdLen);
   if (rc != 1)
       return 10;

   /*
    * After command line is terminated with <CR>, the device responds sending a 
    * four character sequence prompt: <CR><LF><greater_than><space>
    */

   rc = readByteSeq(GSM_serRxBuff, 4);
   if (rc != 4)
   {
       printf("\n No resp to AT+CMGS !!\n");
       return 11;
   }

#if 0
   printf("\n 0[0x%x] 1[0x%x] 2[0x%x] 3[0x%x] \n",
          GSM_serRxBuff[0], 
          GSM_serRxBuff[1], 
          GSM_serRxBuff[2], 
          GSM_serRxBuff[3]); 

   printf("\n resp <%s>  \n", GSM_serRxBuff + 2);
#endif

   /*
    * If message is successfully sent to the network, then the result is sent in 
    * the format: +CMGS: <mr>
    * where
    * <mr> - message reference number; 3GPP TS 23.040 TP-Message-Reference in 
    * integer format.
    * Note: if message sending fails for some reason, an error code is reported.
    */
   if (GSM_serRxBuff[0] != 13
       || GSM_serRxBuff[1] != 10
       || GSM_serRxBuff[2] != '>'
       || GSM_serRxBuff[3] != 0x20)
   {
       printf(" Failed !! \n");
       return 12;
   }

   printf(" Done (%s) \n", GSM_serRxBuff + 2);

   printf("\n Sending SMS text <%s> to modem ... ", msg);
   cmdLen = sprintf(GSM_serTxBuff, "%s", msg);
   rc = GSM_writePort(GSM_serTxBuff, cmdLen);
   if (rc != 1)
   {
       printf("\nGSM_writePort() failed !!\n");
       return 13;
   }

   printf(" Done \n");

   printf("\n Triggering SMS transmission ... ");
  
   GSM_serTxBuff[0] = SMS_TX_CMD; 
   rc = GSM_writePort(GSM_serTxBuff, 1);
   if (rc != 1)
   {
       printf("\nGSM_writePort() failed !!\n");
       return 14;
   }

#if 0
   for (;;)
   {
      unsigned char c;

      if (readByte(&c) != 1)
      {
          if (errno != EAGAIN)
              break;
      }
      else
          printf("\n %0x \n", c);
   }
#endif

      
   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 15;

   if (sscanf(GSM_serRxBuff + 2, "+CMGS: %d", &refNr) == 1)
   {
       printf(" SMS sent (ref nr : %d) \n", refNr);
   }
   else
   {
       printf(" failed !! \n");
       return 16;
   }

   rc = __getResp(GSM_serRxBuff, SER_BUFF_LEN);
   if (rc < 0)
       return 17;
   
   // printf(" %s \n", GSM_serRxBuff + 2);

   return 0;
}

#endif
