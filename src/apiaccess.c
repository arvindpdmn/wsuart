#ifdef API_ENA

#define __USE_XOPEN
#define _GNU_SOURCE

#include <apiaccess.h>
#include <apicache.h>
#include <logger.h>
#include <util.h>
#include <mac_defs.h>

#ifndef REMOTE_DB
#define API_HOST_ADDR "127.0.0.1/wisight"
#else
#define API_HOST_ADDR "mysql.server261.com"
#endif

#define CMD_BUFF_LEN 1024
#define API_URL ("http://" API_HOST_ADDR "/api.php")
#define API_TOKEN ("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE0NjI4NjA2MDcsImp0aSI6IkpFanY1YWxkU1dRYmNkbXlwUHBzclZodEUzbHF6N0hRNWZocU5RdXR3U3M9IiwiaXNzIjoid2lzZW5zZS13aXNpZ2h0IiwibmJmIjoxNDYyODYwNjA3LCJleHAiOjE1NTc0Njg2MDcsImRhdGEiOnsidXNlcmlkIjoiNTgiLCJud2tzIjoiNzQ1NSA0OTIzIDc2NTQiLCJwZXJtcyI6InJ3IHJ3IHJ3In19.zShypsr0z2nC1ATH4B9oF46FK_o0b922Lt3zKadaO8BrItLSsy1mdNjpwwE7ZOVGiHa3Bjii-FqSshWV_3RWSg")

static UINT16_t PanId; // TODO Support multiple PAN

CURLM* initApi(void)
{
#ifdef API_CACHE_ENA
	initApiCache();
#endif

	curl_global_init(CURL_GLOBAL_ALL);
	CURLM *mcurl = curl_multi_init();
	curl_multi_setopt(mcurl, CURLMOPT_PIPELINING, 1);
	return mcurl;
}

void endApi(CURLM *mcurl)
{
	curl_multi_cleanup(mcurl);
	curl_global_cleanup();
}

void addHandle(CURLM *mcurl, CURL *curl)
{
	//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
	curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
	curl_easy_setopt(curl, CURLOPT_TCP_KEEPIDLE, 120L);
	curl_easy_setopt(curl, CURLOPT_TCP_KEEPINTVL, 60L);
	curl_multi_add_handle(mcurl, curl);
}

void removeHandle(struct CURLMsg *msg)
{
	return;
}

void printErrAndExit(CURLcode res, unsigned int lineNum)
{
	LOG(LOG_FATAL, "%d: %s", lineNum, curl_easy_strerror(res));
	exit(1);
}

void printErr(CURLcode res, unsigned int lineNum)
{
	LOG(LOG_FATAL, "%d: %s", lineNum, curl_easy_strerror(res));
}

static void init_string(String *s) {
	s->len = 0;
	s->ptr = malloc(s->len+1);
	if (s->ptr == NULL) {
		fprintf(stderr, "malloc() failed\n");
		exit(1);
	}
	s->ptr[0] = '\0';
}

static size_t writefunc(void *ptr, size_t size, size_t nmemb, String *s)
{
	size_t new_len = s->len + size*nmemb;
	s->ptr = realloc(s->ptr, new_len+1);
	if (s->ptr == NULL) {
		fprintf(stderr, "realloc() failed\n");
		exit(1);
	}
	memcpy(s->ptr+s->len, ptr, size*nmemb);
	s->ptr[new_len] = '\0';
	s->len = new_len;

	return size*nmemb;
}

static struct curl_slist* addToken(CURL *curl)
{
	char cmdBuff[CMD_BUFF_LEN];

	struct curl_slist *headers = NULL;
	sprintf(cmdBuff, "Authorization: Token %s", API_TOKEN);
	headers = curl_slist_append(headers, cmdBuff);

	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	return headers;
}

static void closeTransfers(CURLM *mcurl)
{
	struct CURLMsg *m;
	do {
		int msgq = 0;
		m = curl_multi_info_read(mcurl, &msgq);
		if(m && (m->msg == CURLMSG_DONE)) {
			CURL *e = m->easy_handle;
			curl_multi_remove_handle(mcurl, e);
			curl_easy_cleanup(e);
		}
	} while(m);
}

static UINT8_t apiPost(CURLM *mcurl, char *getParams, char *postData, UINT8_t exitIfErr, const char *caller, String *rsp)
{
	char cmdBuff[CMD_BUFF_LEN];

	if (rsp)
		LOG(LOG_DBG_MEDIUM, "GET: %s; POST: %s; Caller: %s(); PtrLen: %zu", getParams, postData, caller, rsp->len);
	else
		LOG(LOG_DBG_MEDIUM, "GET: %s; POST: %s; Caller: %s()", getParams, postData, caller);

	// Close any previous completed transfers
	closeTransfers(mcurl);

	CURLcode res;
	CURL *curl = curl_easy_init();
	struct curl_slist *headers = addToken(curl);

	if (strlen(getParams))
		sprintf(cmdBuff, "%s?nw=%04X&%s", API_URL, PanId, getParams);
	else
		sprintf(cmdBuff, "%s?nw=%04X", API_URL, PanId);
	curl_easy_setopt(curl, CURLOPT_URL, cmdBuff);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData);
	if (rsp) {
		// Caller expects content in the response
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, rsp);
	}
	addHandle(mcurl, curl);
	
	int numRunning = 0;
	res = curl_multi_perform(mcurl, &numRunning);
	if (res != CURLE_OK) {
		if (exitIfErr) printErrAndExit(res, __LINE__);
		else {
			printErr(res, __LINE__);
			curl_slist_free_all(headers);
			return 1;
		}
	}

	curl_slist_free_all(headers);
	return 0;
}

static String* apiGet(CURLM *mcurl, char *getParams, UINT8_t exitIfErr, const char *caller, UINT8_t useCache)
{
	char cmdBuff[CMD_BUFF_LEN];

	LOG(LOG_DBG_MEDIUM, "GET: %s; Caller: %s()", getParams, caller);

#ifdef API_CACHE_ENA
	// See if this is already in cache
	if (useCache) {
		const char *rspStr = getFromApiCache(PanId, getParams);
		if (rspStr) {
			String *rsp = malloc(sizeof(String));
			init_string(rsp);
			writefunc((void*)rspStr, 1, strlen(rspStr), rsp);
			return rsp;
		}
	}
#endif

	CURLcode res;
	CURL *curl = curl_easy_init();

	String *rsp = malloc(sizeof(String));
	init_string(rsp);

	struct curl_slist *headers = addToken(curl);
	if (strlen(getParams))
		sprintf(cmdBuff, "%s?nw=%04X&%s", API_URL, PanId, getParams);
	else
		sprintf(cmdBuff, "%s?nw=%04X", API_URL, PanId);
	curl_easy_setopt(curl, CURLOPT_URL, cmdBuff);
	curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, rsp);

	res = curl_easy_perform(curl);
	if (res != CURLE_OK) {
		free(rsp->ptr);
		free(rsp);
		if (exitIfErr) printErrAndExit(res, __LINE__);
		else {
			printErr(res, __LINE__);
			curl_easy_cleanup(curl);
			curl_slist_free_all(headers);
			return NULL;
		}
	}

#ifdef API_CACHE_ENA
	if (useCache) {
		addToApiCache(PanId, getParams, rsp->ptr);
	}
#endif

	curl_easy_cleanup(curl);
	curl_slist_free_all(headers);
	return rsp;
}

UINT8_t updateNodeInfo(CURLM *mcurl, NodeInfo *nodeInfo_p)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(nodeInfo_p->macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "name=%s&location=%s&macAddr=%s&macShortAddr=%04X&macCapability=%02X&pushPeriod=%u&state=Associated",
			macAddrStr+12, "Unknown", macAddrStr, nodeInfo_p->macShortAddr, nodeInfo_p->macCapability, nodeInfo_p->pushPeriod);

	free(macAddrStr);

	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT16_t readPushPeriod(CURLM *mcurl, UINT8_t *macAddr, UINT8_t useCache)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "node=%s", macAddrStr);

	free(macAddrStr);

	UINT16_t pushPeriod = 0;
	String *rsp = apiGet(mcurl, cmdBuff, 0, __func__, useCache);
	if (rsp) {
		json_error_t error;
		json_t *root = json_loads(rsp->ptr, 0, &error);
		const char *value = json_string_value(json_object_get(root, "pushPeriod"));
		if (value) pushPeriod = atoi(value);
		json_decref(root);
		free(rsp->ptr);
		free(rsp);
	}
	return pushPeriod;
}

UINT8_t updateNodeSensors(CURLM *mcurl, UINT8_t *macAddr, SensorInfo *sensor_p)
{
	// Do nothing for now: sensors are added to DB when first data comes in
	return 0;
}

UINT8_t updateSensorInfo(CURLM *mcurl, UINT8_t *macAddr, SensorInfo *sensor_p)
{
	return 0; // let web service take care of hard coding sensor info

	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff,
			"macAddr=%s&sensorId=%u&manufacturer=%s&partNum=%s&"
			"activeTime=%s&activePwr=%s&standbyPwr=%s&opMode=%u&"
			"pushPeriod=%s&alarmPeriod=%s&measure=%s&mode=%s&"
			"type=%s&unit=%s&scale=%s&minVal=%d&maxVal=%d&"
			"thresholdLow=%d&thresholdHigh=%d&state=%s",
			macAddrStr, sensor_p->sensorId, sensor_p->manufacturer, sensor_p->partNum,
			sensor_p->activeTime, sensor_p->activePwr, sensor_p->standbyPwr, sensor_p->opMode,
			sensor_p->pushPeriod, sensor_p->alarmPeriod, sensor_p->measure, sensor_p->mode,
			sensor_p->type, sensor_p->unit, sensor_p->scale, sensor_p->minVal, sensor_p->maxVal,
			sensor_p->thresholdLow, sensor_p->thresholdHigh, sensor_p->state);

	free(macAddrStr);

	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updatePanId(CURLM *mcurl, UINT16_t value)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "panId=%04X", value);
	PanId = value;
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updateNodeCount(CURLM *mcurl, UINT16_t value)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "nodeCount=%u", value);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updateBand(CURLM *mcurl, UINT8_t* str)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "band=%s", str);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updateNwChannel(CURLM *mcurl, UINT32_t value)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "channel=%lu", value);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT16_t readChannelNumber(CURLM *mcurl)
{
	UINT16_t channelNum = 0;
	String *rsp = apiGet(mcurl, "", 0, __func__, 1);
	if (rsp) {
		json_error_t error;
		json_t *root = json_loads(rsp->ptr, 0, &error);
		const char *value = json_string_value(json_object_get(root, "channel"));
		if (value && atoi(value)>BASE_FREQ) channelNum = (atoi(value)-BASE_FREQ)/CHANNEL_SPACING; // Hz to channel number
		else channelNum = 0;
		json_decref(root);
		free(rsp->ptr);
		free(rsp);
	}
	return channelNum;
}

UINT8_t updateModulation(CURLM *mcurl, UINT8_t* str)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "modulation=%s", str);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updateRadioBaudRate(CURLM *mcurl, UINT32_t value)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "radioBaudRate=%lu", value);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updateCoordExtAddr(CURLM *mcurl, UINT8_t *macAddr)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "name=LPWMN Coord&macAddr=%s&macShortAddr=%04X&state=Active",
			macAddrStr, PAN_COORD_SHORT_ADDR);

	free(macAddrStr);

	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

char* readCoordExtAddr(CURLM *mcurl)
{
	char *retStr = NULL;
	String *rsp = apiGet(mcurl, "", 0, __func__, 1);
	if (rsp) {
		json_error_t error;
		json_t *root = json_loads(rsp->ptr, 0, &error);
		const char *value = json_string_value(json_object_get(root, "coordMacAddr"));
		if (value) {
			retStr = malloc(strlen(value)+1);
			strcpy(retStr, value);
		}
		json_decref(root);
		free(rsp->ptr);
		free(rsp);
	}
	return retStr;
}

char* readNodeExtAddr(CURLM *mcurl, UINT16_t macShortAddr)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "node=%04X", macShortAddr);

	char *retStr = NULL;
	String *rsp = apiGet(mcurl, cmdBuff, 0, __func__, 1);
	if (rsp) {
		json_error_t error;
		json_t *root = json_loads(rsp->ptr, 0, &error);
		const char *value = json_string_value(json_object_get(root, "macAddr"));
		if (value) {
			retStr = malloc(strlen(value)+1);
			strcpy(retStr, value);
		}
		json_decref(root);
		free(rsp->ptr);
		free(rsp);
	}
	return retStr;
}

UINT8_t updateCoordTxPwr(CURLM *mcurl, UINT16_t value)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "coordTxPower=%d", value);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

static char* getNodeAssocState(UINT8_t value)
{
	static char state[16];
	if (value) strcpy(state,"Enabled");
	else strcpy(state,"Disabled");
	return state;
}

UINT8_t updateNodeAssocState(CURLM *mcurl, UINT8_t value)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "nodeAssocState=%s", getNodeAssocState(value));
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t readNodeAssocState(CURLM *mcurl)
{
	UINT8_t nodeAssocState = 1;
	String *rsp = apiGet(mcurl, "", 0, __func__, 1);
	if (rsp) {
		json_error_t error;
		json_t *root = json_loads(rsp->ptr, 0, &error);
		const char *value = json_string_value(json_object_get(root, "nodeAssocState"));
		if (strcmp(value,"Disabled")) nodeAssocState = 0;
		json_decref(root);
		free(rsp->ptr);
		free(rsp);
	}
	return nodeAssocState;
}

UINT8_t updateNodeBuildDate(CURLM *mcurl, UINT8_t *macAddr, char *value_p)
{
	char cmdBuff[CMD_BUFF_LEN], dateStr[16];
	struct tm dt;
	strptime(value_p, "%b %d %Y", &dt);
	strftime(dateStr, sizeof(dateStr), "%Y-%m-%d", &dt);
	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	sprintf(cmdBuff, "macAddr=%s&buildDate=%s", macAddrStr, dateStr);
	free(macAddrStr);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updateNodeBuildTime(CURLM *mcurl, UINT8_t *macAddr, char *value_p)
{
	char cmdBuff[CMD_BUFF_LEN];
	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	sprintf(cmdBuff, "macAddr=%s&buildTime=%s", macAddrStr, value_p);
	free(macAddrStr);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t addToNodeWhiteList(CURLM *mcurl, UINT8_t *macAddr)
{
	LOG(LOG_WARN, "Call to addToNodeWhiteList() unexpected. Function not implemented.");
	return 0;
}

UINT8_t insertEvent(CURLM *mcurl, UINT8_t *macAddr, const char *severity, const char *type, char *msg)
{
	char cmdBuff[CMD_BUFF_LEN];
	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	sprintf(cmdBuff, "macAddr=%s&severity=%s&type=%s&msg=%s", macAddrStr, severity, type, msg);
	free(macAddrStr);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updateRssiAndCorrelation(CURLM *mcurl, UINT8_t *macAddr, SINT8_t rssi, UINT8_t corr)
{
	char cmdBuff[CMD_BUFF_LEN];
	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	sprintf(cmdBuff, "macAddr=%s&lastHopRssi=%d&lastHopCorr=%u", macAddrStr, rssi, corr);
	free(macAddrStr);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t updateAllSensorData(CURLM *mcurl, UINT8_t *macAddr, SINT8_t rssi, UINT8_t corr, UINT8_t numSensors, SensorDataStore *ds)
{
	unsigned int i;
	char cmdBuff[CMD_BUFF_LEN + 64*numSensors];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	char *coordMacAddrStr = readCoordExtAddr(mcurl);

	if (strncmp(macAddrStr,coordMacAddrStr,strlen(macAddrStr))==0) {
		LOG(LOG_WARN, "Sensor data from PAN coordinator is unexpected.");
		free(coordMacAddrStr);
		free(macAddrStr);
		return 1;
	}
	else {
		sprintf(cmdBuff, "macAddr=%s&lastHopRssi=%d&lastHopCorr=%u", macAddrStr, rssi, corr);
		free(coordMacAddrStr);
		free(macAddrStr);
		for (i=0; i<numSensors; i++) {
			sprintf(cmdBuff+strlen(cmdBuff), "&sensorId[]=%u&data[]=%ld", ds[i].type, (SINT32_t)(ds[i].convertedValue));
		}
		return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
	}
}

UINT8_t updateSensorData(CURLM *mcurl, UINT8_t *macAddr, UINT8_t sensorId, UINT32_t value)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	char *coordMacAddrStr = readCoordExtAddr(mcurl);

	if (strncmp(macAddrStr,coordMacAddrStr,strlen(macAddrStr))==0) {
		LOG(LOG_WARN, "Sensor data from PAN coordinator is unexpected.");
		free(coordMacAddrStr);
		free(macAddrStr);
		return 1;
	}
	else {
		sprintf(cmdBuff, "macAddr=%s&sensorId=%u&data=%ld", macAddrStr, sensorId, (SINT32_t)value);
		free(coordMacAddrStr);
		free(macAddrStr);
		return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
	}
}

UINT8_t updateRouteInfo(CURLM *mcurl, UINT8_t *macAddr)
{
	char cmdBuff[CMD_BUFF_LEN];
	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	sprintf(cmdBuff, "action=updateRoute&macAddr=%s", macAddrStr);
	free(macAddrStr);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

char* readNodeWhiteList(CURLM *mcurl)
{
	String *rsp = apiGet(mcurl, "node=whitelist", 0, __func__, 1);
	if (rsp) {
		json_error_t error;
		json_t *root = json_loads(rsp->ptr, 0, &error);
		unsigned char numNodes = json_array_size(root);
		char *whiteList = NULL;
		if (numNodes) {
			whiteList = malloc(1+8*numNodes); // numNodes(1 byte) + macAddr(8 bytes)*numNodes
			*whiteList = numNodes;
			unsigned int i;
			for(i=0; i < numNodes; i++) {
				const char *value = json_string_value(json_object_get(json_array_get(root, i), "whiteMacAddr"));
				UINT8_t *macAddr = parseString(value);
				memcpy(whiteList+1+i*8, macAddr, 8);
				free(macAddr);
			}
		}
		json_decref(root);
		free(rsp->ptr);
		free(rsp);
		return whiteList;
	}
	return NULL;
}

String* readRule(CURLM *mcurl, UINT8_t *macAddr)
{
	char cmdBuff[CMD_BUFF_LEN];
	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	sprintf(cmdBuff, "node=%s&sensor=all", macAddrStr); // since API works with sensorUid, get all sensors
	free(macAddrStr);
	return apiGet(mcurl, cmdBuff, 0, __func__, 1);
}

UINT8_t updateRuleNotifiedTs(CURLM *mcurl, UINT16_t ruleUid)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "ruleUid=%d", ruleUid);
	return apiPost(mcurl, "", cmdBuff, 0, __func__, NULL);
}

UINT8_t getAllPendingActions(CURLM *mcurl, unsigned char *numActions, PendingAction **actions)
{
	String *rsp = apiGet(mcurl, "actions=1", 0, __func__, 0);
	if (rsp->len) {
		json_error_t error;
		json_t *root = json_loads(rsp->ptr, 0, &error);
		*numActions = json_array_size(root);
		if (*numActions) {
			*actions = malloc(*numActions * sizeof(PendingAction));
			unsigned int i;
			for(i=0; i < *numActions; i++) {
				const char *type = json_string_value(json_object_get(json_array_get(root, i), "type"));
				strcpy((*actions+i)->type, type);
				const char *data = json_string_value(json_object_get(json_array_get(root, i), "data"));
				strcpy((*actions+i)->data, data);
			}
		}
		json_decref(root);
	}
	free(rsp->ptr);
	free(rsp);
	return 0;
}

#endif
