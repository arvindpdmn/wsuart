/*
 * sensor.c
 *
 *  Created on: Apr 8, 2015
 *      Author: AP
 */

#include <stdlib.h>
#include <sensor.h>
#include <math.h>

// TODO Implement sensor info by querying the nodes: hard coded for now
SensorInfo fixedSensors[NUM_SENSORS] =
{
	{
		PLTFRM_ON_CHIP_VCC_SENSOR_DEV_ID, "TI", "MSP430", "125 us", "-", "-", DIS_SENSOR_OPN_MODE_PUSH_PERIODIC, "3 sec", "-",
		"Instantaneous", "Digital", "Voltage", "Volt", "Milli", 0, 0, 0, 0, "Unknown"
	},
	{
		PLTFRM_LM75B_1_DEV_ID, "NXP", "LM75B", "125 us", "-", "-", DIS_SENSOR_OPN_MODE_PUSH_PERIODIC, "3 sec", "-",
		"Instantaneous", "Digital", "Temperature", "Celsius", "Centi", 0, 0, 0, 0, "Unknown"
	},
	{
		PLTFRM_TSL45315_1_DEV_ID, "AMS-TAOS", "TSL45315", "400 ms", "-", "-", DIS_SENSOR_OPN_MODE_PUSH_PERIODIC, "3 sec", "-",
		"Instantaneous", "Digital", "Luminance", "Lux", "Unit", 0, 0, 0, 0, "Unknown"
	},
	{
		PLTFRM_CHIRP_PWLA_1_DEV_ID, "CERN", "CHIRP_PWLA", "400 ms", "-", "-", DIS_SENSOR_OPN_MODE_PUSH_PERIODIC, "3 sec", "-",
		"Instantaneous", "Digital", "Moisture", "Number", "Unit", 0, 0, 0, 0, "Unknown"
	},
	{
		PLTFRM_MP3V5050GP_1_DEV_ID, "Freescale", "MP3V5050GP", "400 ms", "-", "-", DIS_SENSOR_OPN_MODE_PUSH_PERIODIC, "3 sec", "-",
		"Instantaneous", "Analog", "Pressure", "Inch", "Milli", 0, 0, 0, 0, "Unknown"
	},
	{
		PLTFRM_MP3V5004GP_1_DEV_ID, "Freescale", "MP3V5004GP", "400 ms", "-", "-", DIS_SENSOR_OPN_MODE_PUSH_PERIODIC, "3 sec", "-",
		"Instantaneous", "Analog", "Pressure", "Inch", "Milli", 0, 0, 0, 0, "Unknown"
	},
	{
		PLTFRM_WSMS100_1_DEV_ID, "WiSense", "WSMS100", "400 ms", "-", "-", DIS_SENSOR_OPN_MODE_PUSH_PERIODIC, "3 sec", "-",
		"Instantaneous", "Digital", "Moisture", "Percent", "Centi", 0, 0, 0, 0, "Unknown"
	},
	{
		PLTFRM_NTC_THERMISTOR_1_DEV_ID, "Vishay", "NTCALUG02A", "400 ms", "-", "-", DIS_SENSOR_OPN_MODE_PUSH_PERIODIC, "3 sec", "-",
		"Instantaneous", "Analog", "Temperature", "Celsius", "Centi", 0, 0, 0, 0, "Unknown"
	}
};

SensorInfo *getSensorInfo(UINT8_t sensorId)
{
	unsigned int i;

	for (i=0; i<NUM_SENSORS; i++) {
		if (fixedSensors[i].sensorId == sensorId)
			return &fixedSensors[i];
	}

	return NULL;
}

UINT32_t convertSensorValue(UINT8_t sensorId, UINT32_t value, UINT32_t refVcc)
{
	UINT32_t convertedVal = value;
    float temp;
	double _r, _rl;
	int b25by85;

	switch (sensorId) {
		case PLTFRM_MP3V5050GP_1_DEV_ID:
			// Vout = Vs (P x 0.018 + 0.04)
			temp = ((float)value/refVcc - 0.04)/0.018;
			if (temp<0) temp = 0;
			convertedVal = 4015*temp; // kPa to inches of water
			break;
		case PLTFRM_MP3V5004GP_1_DEV_ID:
			// Vout = Vs (P x 0.2 + 0.2)
			temp = ((float)value/refVcc - 0.2)/0.2;
			if (temp<0) temp = 0;
			convertedVal = 4015*temp; // kPa to inches of water
			break;
		case PLTFRM_NTC_THERMISTOR_1_DEV_ID:
		case PLTFRM_NTC_THERMISTOR_2_DEV_ID:
			if (sensorId==PLTFRM_NTC_THERMISTOR_1_DEV_ID) {
				_r = NTC_THERM_NTCALUG02A_R25_VAL;
				b25by85 = NTC_THERM_NTCALUG02A_B_25_85_VAL;
			}
			else {
				_r = NTC_THERM_103AT_4_R25_VAL;
				b25by85 = NTC_THERM_103AT_4_B_25_85_VAL;
			}
			_r /= (double)value;
			_rl = log(_r);
			_rl /= b25by85;
			_r = 1;
			_r /= 25 + 273.15;
			_r -= _rl;
			_r = (1 / _r);
			_r -= 273.15;
			convertedVal = _r*100; // TODO Scale not sent by node
            break;
		default:
			break;
	}

	return convertedVal;
}

