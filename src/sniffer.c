#ifdef SNIFFER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <mac_defs.h>
#include <phy_defs.h>
#include <gw.h>
#include <sniffer.h>

char *MAC_frameTypeList[8] =
{
   "BCN", "DATA", "ACK", "CMD",
   "RSVD", "RSVD", "RSVD", "RSVD"
};


char *__macCmdsList[ ] =
{
  "ASSOC_REQ",
  "ASSOC_RESP",
  "DISASSOC_NOTIF",
  "DATA_REQ",
  "LPWMN_ID_CONFLICT_NOTIF",
  "ORPHAN_NOTIF",
  "BCN_REQ",
  "COORD_REALIGNMENT",
  "GTS_REQ"
};

char *__macAssocRespSts[ ] =
{
  "Success",
  "LPWMN at capacity",
  "LPWMN access denied"
};

void __decodeBcnFrame(unsigned char *pdu_p, int pduLen)
{
   if (pduLen >= MAC_BCN_PYLD_LEN)
   {
       printf("Assoc-Allowed<%c>  Sender-is-Coord<%c> \n",
              pdu_p[1] & MAC_SF_SPEC_ASSOC_PERMIT_BIT_SHIFT_MSK ? 'y' : 'n',
              pdu_p[1] & MAC_SF_SPEC_IS_COORD_BIT_SHIFT_MSK ? 'y' : 'n');
   }
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void __decodeAckFrame(unsigned char *pdu_p, int pduLen)
{
   if (pduLen >= (MAC_ACK_PDU_RSSI_FIELD_LEN + MAC_ACK_PDU_LQI_FIELD_LEN))
   {
       int rssi = *((char *)pdu_p), lqi = pdu_p[1];

       printf("rssi<%d>  lqi<%u> \n", rssi, lqi);
   }
}


/*
 ********************************************************************
 *
 *
 *
 *
 ********************************************************************
 */
void __decodeCmdFramePyld(unsigned char *pdu_p, int pduLen)
{
   if (pduLen >= MAC_CMD_ID_FIELD_LEN)
   {
       unsigned char cmdId = *pdu_p;
       pdu_p += MAC_CMD_ID_FIELD_LEN;
       pduLen -= MAC_CMD_ID_FIELD_LEN;

       if (cmdId < MAC_CMD_ASSOCIATION_REQ || cmdId > MAC_CMD_GTS_REQUEST)
       {
           printf("\nCmd-Type<Unknown (%u)>\n", cmdId);
       }
       else
       {
           printf("\nCmd-Type<%s>\n", __macCmdsList[cmdId - 1]);
       }

       switch (cmdId)
       {
          case MAC_CMD_ASSOCIATION_REQ:
               {
                  // 1 byte command id, 1 byte capability info
                  if (pduLen > 0)
                  {
                      unsigned char capInfo = *pdu_p;
                      printf("Cap Info: Dev-Type<%s> AC-Pwr<%c> Rx-On-Idle<%c> \n",
                             capInfo & (1 << MAC_ASSOC_REQ_CAP_INO_DEV_TYPE_BIT_NR) ? "FFD" : "RFD",
                             capInfo & (1 << MAC_ASSOC_REQ_CAP_INO_POWER_SOURCE_BIT_NR) ? 'y' : 'n',
                             capInfo & (1 << MAC_ASSOC_REQ_CAP_INO_RX_ON_IDLE_BIT_NR) ? 'y' : 'n');

                  }
               }
               break;

          case MAC_CMD_ASSOCIATION_RESP:
               {
                  // 1 byte command id, 2 byte short address, 1 byte association status
                  if (pduLen >= (MAC_SHORT_ADDR_LEN + MAC_ASSOC_RESP_ASSOC_STS_FIELD_LEN))
                  {
                      unsigned char assocSts = *(pdu_p + MAC_SHORT_ADDR_LEN);
                      printf("Assoc Sts<%s> ",
                             assocSts <= MAC_ASSOC_STS_LPWMN_ACCESS_DENIED ? \
                             __macAssocRespSts[assocSts] : "Unknown");
                      if (assocSts == MAC_ASSOC_STS_SUCCESS)
                      {
                          unsigned short shortAddr = *(pdu_p + 1);
                          shortAddr = (shortAddr << 8) | *(pdu_p);
                          printf(" Allocated Short Address<0x%04x> \n", shortAddr);
                      }
                      else
                          printf("\n");
                  }
               }
               break;

          default:
               {

               }
               break;
       }
   }
}

void __decodePdu(unsigned char *pdu_p, int pduLen)
{
   unsigned char frameType, secEna, framePending,
                 ackRequired, panIdCompressed, destAddrMode,
                 srcAddrMode;
   unsigned int frameSeqNr, errFlag = 0;
   unsigned short destLPWMNId, destShortAddr, srcLPWMNId, srcShortAddr;
   unsigned char *destExtAddr_p = NULL;
   unsigned char *srcExtAddr_p = NULL;

   if (pduLen < MAC_PDU_HDR_FC_FIELD_LEN)
       return;

   frameType = (*pdu_p) & MAC_FC_FRAME_TYPE_BITS_SHIFT_MSK;
   secEna = (*pdu_p) & MAC_FC_SEC_ENA_BIT_SHIFT_MSK;
   framePending = (*pdu_p) & MAC_FC_PENDING_BIT_SHIFT_MSK;
   ackRequired = (*pdu_p) & MAC_FC_ACK_REQ_BIT_SHIFT_MSK;
   panIdCompressed = (*pdu_p) & MAC_FC_LPWMN_ID_COMP_BIT_SHIFT_MSK;


   if (frameType > MAC_FRAME_TYPE_CMD)
       return;

   printf("\nType<%s> Sec<%c> FP<%c> AR<%c> PIC<%c> ",
          MAC_frameTypeList[frameType],
          secEna ? 'y' : 'n',
          framePending ? 'y' : 'n',
          ackRequired ? 'y' : 'n',
          panIdCompressed ? 'y' : 'n');
   pdu_p ++;

#ifndef SNIFFER_SUPPORTS_SEC
   if (secEna)
       return;
#endif

   destAddrMode = (*pdu_p);
   destAddrMode >>= MAC_FC_DAM_BITS_SHIFT;
   destAddrMode &= MAC_FC_DAM_BITS_MSK;

   srcAddrMode = (*pdu_p);
   srcAddrMode >>= MAC_FC_SAM_BITS_SHIFT;
   srcAddrMode &= MAC_FC_SAM_BITS_MSK;

   if (destAddrMode == MAC_ADDRESS_MODE_RSVD
       || srcAddrMode == MAC_ADDRESS_MODE_RSVD)
   {
       return;
   }

   pdu_p ++;
   pduLen -= MAC_PDU_HDR_FC_FIELD_LEN;

   if (pduLen < MAC_PDU_HDR_SEQ_NR_FIELD_LEN)
       return;

#if (MAC_PDU_HDR_SEQ_NR_FIELD_LEN == 4)
   frameSeqNr = pdu_p[3];
   frameSeqNr <<= 8;
   frameSeqNr |= pdu_p[2];
   frameSeqNr <<= 8;
   frameSeqNr |= pdu_p[1];
   frameSeqNr <<= 8;
   frameSeqNr |= pdu_p[0];
#elif (MAC_PDU_HDR_SEQ_NR_FIELD_LEN == 2)
   frameSeqNr = pdu_p[1];
   frameSeqNr <<= 8;
   frameSeqNr |= pdu_p[0];
#elif (MAC_PDU_HDR_SEQ_NR_FIELD_LEN == 1)
   frameSeqNr = pdu_p[0];
#else
#error mac pdu header seq number field len not defined !!
#endif

   printf("Seq-Nr<%u> ", frameSeqNr);

   pdu_p += MAC_PDU_HDR_SEQ_NR_FIELD_LEN;
   pduLen -= MAC_PDU_HDR_SEQ_NR_FIELD_LEN;

   switch (destAddrMode)
   {
       case MAC_ADDRESS_MODE_SHORT_ADDR:
       case MAC_ADDRESS_MODE_EXTENDED_ADDR:
            {
               if (pduLen >= MAC_LPWMN_ID_LEN)
               {
                   destLPWMNId = pdu_p[1];
                   destLPWMNId = (destLPWMNId << 8) | pdu_p[0];
                   pdu_p += MAC_LPWMN_ID_LEN;
                   pduLen -= MAC_LPWMN_ID_LEN;

                   if (destAddrMode == MAC_ADDRESS_MODE_SHORT_ADDR)
                   {
                       if (pduLen >= MAC_SHORT_ADDR_LEN)
                       {
                           destShortAddr = pdu_p[1];
                           destShortAddr = (destShortAddr << 8) | pdu_p[0];
                           pdu_p += MAC_SHORT_ADDR_LEN;
                           pduLen -= MAC_SHORT_ADDR_LEN;
                       }
                       else
                           errFlag = 1;
                   }
                   else
                   {
                       if (pduLen >= MAC_EXT_ADDR_LEN)
                       {
                           destExtAddr_p = pdu_p;
                           pdu_p += MAC_EXT_ADDR_LEN;
                           pduLen -= MAC_EXT_ADDR_LEN;
                       }
                       else
                           errFlag = 1;
                   }
               }
               else
                   errFlag = 1;
            }
            break;

       case MAC_ADDRESS_MODE_NO_ADDR:
       default:
            break;
   }

   if (errFlag)
       return;

   if (destAddrMode != MAC_ADDRESS_MODE_NO_ADDR)
   {
       printf("\nDest LPWMN-Id<0x%04x> ", destLPWMNId);

       if (destAddrMode == MAC_ADDRESS_MODE_SHORT_ADDR)
           printf("/ Dest Short<0x%04x>", destShortAddr);
       else
       {
           printf("/ Ext<%x:%x:%x:%x:%x:%x:%x:%x:>",
                  destExtAddr_p[0], destExtAddr_p[1], destExtAddr_p[2], destExtAddr_p[3],
                  destExtAddr_p[4], destExtAddr_p[5], destExtAddr_p[6], destExtAddr_p[7]);
       }
   }
   else
       printf("\nDest: None");

   switch (srcAddrMode)
   {
       case MAC_ADDRESS_MODE_SHORT_ADDR:
       case MAC_ADDRESS_MODE_EXTENDED_ADDR:
            {
               if (panIdCompressed)
               {
                   if (destAddrMode == MAC_ADDRESS_MODE_NO_ADDR)
                       errFlag = 1;
                   else
                   {
                       srcLPWMNId = destLPWMNId;
                   }
               }
               else
               {
                   if (pduLen >= MAC_LPWMN_ID_LEN)
                   {
                       srcLPWMNId = pdu_p[1];
                       srcLPWMNId = (srcLPWMNId << 8) | pdu_p[0];
                       pdu_p += MAC_LPWMN_ID_LEN;
                       pduLen -= MAC_LPWMN_ID_LEN;
                   }
                   else
                       errFlag = 1;
               }

               if (errFlag)
                   break;

               if (srcAddrMode == MAC_ADDRESS_MODE_SHORT_ADDR)
               {
                   if (pduLen >= MAC_SHORT_ADDR_LEN)
                   {
                       srcShortAddr = pdu_p[1];
                       srcShortAddr = (srcShortAddr << 8) | pdu_p[0];
                       pdu_p += MAC_SHORT_ADDR_LEN;
                       pduLen -= MAC_SHORT_ADDR_LEN;
                   }
                   else
                       errFlag = 1;
               }
               else
               {
                   if (pduLen >= MAC_EXT_ADDR_LEN)
                   {
                       srcExtAddr_p = pdu_p;
                       pdu_p += MAC_EXT_ADDR_LEN;
                       pduLen -= MAC_EXT_ADDR_LEN;
                   }
                   else
                       errFlag = 1;
               }
            }
            break;

       case MAC_ADDRESS_MODE_NO_ADDR:
       default:
            break;
   }

   if (errFlag)
       return;

   if (srcAddrMode != MAC_ADDRESS_MODE_NO_ADDR)
   {
	   printf("\nSrc LPWMN-Id<0x%04x> ", srcLPWMNId);

	  if (srcAddrMode == MAC_ADDRESS_MODE_SHORT_ADDR)
		  printf("/ Src Short<0x%04x>\n", srcShortAddr);
	  else
	  {
		  printf("/ Ext<%x:%x:%x:%x:%x:%x:%x:%x:>\n",
				 srcExtAddr_p[0], srcExtAddr_p[1], srcExtAddr_p[2], srcExtAddr_p[3],
				 srcExtAddr_p[4], srcExtAddr_p[5], srcExtAddr_p[6], srcExtAddr_p[7]);
	  }
   }
   else
   {
       printf("\nSrc LPWMN-Id<None> / Src Short<None>");
   }

   switch (frameType)
   {
      case MAC_FRAME_TYPE_BEACON:
           __decodeBcnFrame(pdu_p, pduLen);
           break;

      case MAC_FRAME_TYPE_DATA:
           break;

      case MAC_FRAME_TYPE_ACK:
           __decodeAckFrame(pdu_p, pduLen);
           break;

      case MAC_FRAME_TYPE_CMD:
           __decodeCmdFramePyld(pdu_p, pduLen);
           break;

      default:
           break;
   }
}

#endif
