#if defined(API_ENA) && defined(API_CACHE_ENA)

#include <apicache.h>
#include <logger.h>

static CacheEntry ApiCache[MAX_CACHE_LEN];

void initApiCache(void)
{
	memset(ApiCache, 0, sizeof(ApiCache));
}

const char* getFromApiCache(UINT16_t panId, const char *getParams)
{
	unsigned char i;
	for (i=0; i<MAX_CACHE_LEN; i++) {
		if (ApiCache[i].isActive && panId==ApiCache[i].panId && strcmp(getParams, ApiCache[i].getParams)==0) {
			ApiCache[i].hits++;
			LOG(LOG_DBG_MEDIUM, "ApiCache hit: %d: %s", ApiCache[i].hits, getParams);
			return ApiCache[i].rspStr;
		}
	}
	return NULL;
}

void addToApiCache(UINT16_t panId, const char *getParams, const char *rspStr)
{
	unsigned char i;
	for (i=0; i<MAX_CACHE_LEN; i++) {
		if (ApiCache[i].isActive==true && strcmp(getParams, ApiCache[i].getParams)==0) {
			strcpy(ApiCache[i].rspStr, rspStr);
			LOG(LOG_DBG_MEDIUM, "ApiCache updated: %s", getParams);
		}
	}

	for (i=0; i<MAX_CACHE_LEN; i++) {
		if (ApiCache[i].isActive==false) {
			ApiCache[i].isActive = true;
			ApiCache[i].panId = panId;
			strcpy(ApiCache[i].getParams, getParams);
			strcpy(ApiCache[i].rspStr, rspStr);
			ApiCache[i].hits = 0;
			return;
		}
	}
	LOG(LOG_WARN, "ApiCache is full.");
}

void removeFromApiCache(UINT16_t panId, const char *getParams)
{
	unsigned char i;
	for (i=0; i<MAX_CACHE_LEN; i++) {
		if (panId==ApiCache[i].panId && strcmp(getParams, ApiCache[i].getParams)==0) {
			memset(&ApiCache[i], 0, sizeof(CacheEntry));
			ApiCache[i].isActive = false;
			LOG(LOG_DBG_MEDIUM, "ApiCache removed: %s", getParams);
		}
	}
}

#endif
