# -*- coding: utf-8 -*-
import sys
import plivo

if len(sys.argv)!=3:
    print('Syntax: {} <mobile> <msg>'.format(sys.argv[0]))
    exit(1)
else:
    mobile, msg = sys.argv[1:]

auth_id = "XXX";
auth_token = "XXX";

p = plivo.RestAPI(auth_id, auth_token)
params = {
    'src': '918123868229',
    'dst' : '91{}'.format(mobile),
    'text' : msg,
    'url' : 'http://wisense.in', # not relevant for now message is sent
    'method' : 'POST'
}

response = p.send_message(params)

# print(str(response))