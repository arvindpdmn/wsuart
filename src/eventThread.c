/*
 * eventThread.c
 *
 *  Created on: Nov 14, 2013
 *      Author: AP
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <logger.h>
#include <eventThread.h>
#include <gw.h>
#include <pktSched.h>
#ifdef API_ENA
#include <apiaccess.h>
#else
#include <database.h>
#endif

#ifdef SNIFFER
#define TCPIP_PORT 3398
#else
#define TCPIP_PORT 3399
#endif

#ifndef GUI_INTERFACE_DB

int initTcpIpServer(void)
{
	/* create a TCP/IP socket */
	int svc;
	if ((svc = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		LOG(LOG_FATAL, "Cannot create socket.");
		exit(1);
	}

	/* allow immediate reuse of the port */
	int sockoptval = 1;
	setsockopt(svc, SOL_SOCKET, SO_REUSEADDR, &sockoptval, sizeof(int));

	/* bind the socket to our source address */
	struct sockaddr_in my_addr;
	memset((char*)&my_addr, 0, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(TCPIP_PORT);
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(svc, (struct sockaddr *)&my_addr, sizeof(my_addr)) < 0) {
		LOG(LOG_FATAL, "Bind failed.");
		exit(1);
	}

	if (listen(svc, 5) < 0) {
		LOG(LOG_FATAL, "Listen failed.");
		exit(1);
	}

	return svc;
}

void listenOnPort(int svc)
{
	int rqst;
	socklen_t alen;
	struct sockaddr_in client_addr;
	while (1) {
		while ((rqst = accept(svc, (struct sockaddr *)&client_addr, &alen)) < 0) {
			if ((errno != ECHILD) && (errno != EINTR)) {
				LOG(LOG_FATAL, "Accept failed.");
				exit(1);
			}
		}

	    UINT8_t buffer[256];
	    bzero(buffer, 256);
	    int n = read(rqst, buffer, 255);
	    if (n < 0) LOG(LOG_WARN, "ERROR reading from socket.");
	    PRINT_BUFFER(LOG_INFO, "--> RxTcpServer", buffer, n);
	}
}

#else

#define CMD_BUFF_LEN 1024

#ifdef API_ENA
extern CURLM *mainCon;
#define evtCon mainCon
#else
MYSQL *evtCon;
#endif

void processAction(PendingAction *pa)
{
	if (pa==NULL) return;

	LOG(LOG_DBG_MEDIUM, "processAction(%s)", pa->type);

	if (strcmp(pa->type,"coord-reset")==0) {
		scheduleHdr(LPWMN_GW_MSG_TYPE_REBOOT_COORD);
	}
	else if (strcmp(pa->type,"nj-ena")==0) {
		scheduleHdr(LPWMN_GW_MSG_TYPE_ENABLE_NWK_JOINS);
	}
	else if (strcmp(pa->type,"nj-dis")==0) {
		scheduleHdr(LPWMN_GW_MSG_TYPE_DISABLE_NWK_JOINS);
	}
	else if (strcmp(pa->type,"wl-add")==0) {
		char *token = strtok(pa->data, ",");
		while (token != NULL) {
			UINT8_t *macAddr = parseString(token);
			token = strtok(NULL, ",");
			schedulePkt(LPWMN_GW_MSG_TYPE_ADD_NODE_TO_WHITE_LIST, macAddr, 8);
			free(macAddr);
		}
	}
	else if (strcmp(pa->type,"wl-del")==0) {
		char *token = strtok(pa->data, ",");
		while (token != NULL) {
			UINT8_t *macAddr = parseString(token);
			token = strtok(NULL, ",");
			schedulePkt(LPWMN_GW_MSG_TYPE_DEL_NODE_FROM_WHITE_LIST, macAddr, 8);
			free(macAddr);
		}
	}
	else if (strcmp(pa->type,"wl-clr")==0) {
		scheduleHdr(LPWMN_GW_MSG_TYPE_DELETE_WHITE_LIST);
	}
	else if (strcmp(pa->type,"cfg-dpi")==0) {
		UINT16_t macShortAddr;
		sscanf(pa->data, "%X", &macShortAddr);
		char *macAddrStr = readNodeExtAddr(evtCon, macShortAddr);
		if (macAddrStr==NULL) return;
		UINT8_t *macAddr = parseString(macAddrStr);
#ifdef API_ENA
		setNodePushPeriod(macShortAddr, readPushPeriod(evtCon, macAddr, 0));
#else
		setNodePushPeriod(macShortAddr, readPushPeriod(evtCon, macAddr));
#endif
		free(macAddrStr);
		free(macAddr);
	}
	else if (strcmp(pa->type,"dioc")==0) {
		// macShortAddr,portNum,pinNum,gpioValue
		char *token = strtok(pa->data, ",");
		UINT16_t macShortAddr;
		sscanf(token, "%X", &macShortAddr);

		int portNum, pinNum, gpioValue;
		token = strtok(NULL, ",");
		sscanf(token, "%d", &portNum);
		token = strtok(NULL, ",");
		sscanf(token, "%d", &pinNum);
		token = strtok(NULL, ",");
		sscanf(token, "%d", &gpioValue);

		// TODO: Remove this hack for a specific node
		/*char *macAddr = readNodeExtAddr(evtCon, macShortAddr);
		if (macAddr==NULL) return;
		if (strcmp(macAddr,"FCC23D000000E2FE")==0)
			gpioValue = gpioValue ? 0 : 1;
		free(macAddr);*/

		UINT16_t pyldLen = 0;
		UINT8_t *payLoad;
		pyldLen = MAC_SHORT_ADDR_LEN
				+ DIS_MSG_TYPE_SZ
				+ DIS_TLV_HDR_SZ
				+ DIS_GPIO_PORT_ID_TLV_SZ
				+ DIS_GPIO_PIN_ID_TLV_SZ
				+ DIS_GPIO_VAL_TLV_SZ;
		payLoad = malloc(pyldLen);

		myhtons(payLoad, macShortAddr);

		UINT16_t off = MAC_SHORT_ADDR_LEN;
		payLoad[off++] = DIS_MSG_TYPE_CTRL_DIGITAL_IO;

		payLoad[off++] = DIS_TLV_TYPE_GPIO_CTRL;
		payLoad[off++] = (DIS_GPIO_PORT_ID_TLV_SZ
							+ DIS_GPIO_PIN_ID_TLV_SZ
							+ DIS_GPIO_VAL_TLV_SZ);

		payLoad[off++] = DIS_TLV_TYPE_GPIO_PORT_ID;
		payLoad[off++] = DIS_GPIO_PORT_ID_FIELD_SZ;
		payLoad[off++] = portNum;

		payLoad[off++] = DIS_TLV_TYPE_GPIO_PIN_ID;
		payLoad[off++] = DIS_GPIO_PIN_ID_FIELD_SZ;
		payLoad[off++] = pinNum;

		payLoad[off++] = DIS_TLV_TYPE_GPIO_VAL;
		payLoad[off++] = DIS_GPIO_VAL_FIELD_SZ;
		payLoad[off++] = gpioValue;

		schedulePkt(LPWMN_GW_MSG_TYPE_RELAY_TO_NODE, payLoad, pyldLen);

		free(payLoad);
	}
}

#ifndef API_ENA
void pollDatabase()
{
	PendingAction *pa;

	while (1) {
		sleep(1);
		pa = getFirstPendingAction(evtCon);
		if (pa) {
			deleteFirstPendingAction(evtCon);
			processAction(pa);
			free(pa);
		}
	}
}
#endif

#endif

#ifndef API_ENA
void waitForEvents(void *ptr)
{
#ifndef GUI_INTERFACE_DB
	int svc = initTcpIpServer();
	listenOnPort(svc);
#else
	// API interface does not use a separate thread
	evtCon = openDbConnection();
	clearAllPendingActions(evtCon);
	pollDatabase();
	closeDbConnection(evtCon);
#endif
}
#endif
