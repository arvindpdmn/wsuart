/*
 * util.c
 *
 *  Created on: Nov 13, 2013
 *      Author: AP
 */
#include <string.h>
#include <util.h>
#include <logger.h>
#include <stdio.h>
#include <stdlib.h>

UINT8_t *myhtons(UINT8_t *buff_p, UINT16_t val)
{
   *(buff_p + 1) = val;
   val >>= 8;
   *(buff_p) = val;
   return (buff_p + 2);
}

UINT16_t myntohs(UINT8_t *buff_p)
{
   UINT16_t u16Val = *buff_p;
   u16Val = (u16Val << 8) | buff_p[1];
   return u16Val;
}

UINT32_t myntohl(UINT8_t *buff_p)
{
   UINT32_t u32Val = *buff_p;
   u32Val <<= 8;
   u32Val |= buff_p[1];
   u32Val <<= 8;
   u32Val |= buff_p[2];
   u32Val <<= 8;
   u32Val |= buff_p[3];
   return u32Val;
}

UINT8_t *myhtonl(UINT8_t *buff_p, UINT32_t val)
{
   *(buff_p + 3) = val;
   val >>= 8;
   *(buff_p + 2) = val;
   val >>= 8;
   *(buff_p + 1) = val;
   val >>= 8;
   *(buff_p) = val;
   return (buff_p + 4);
}

void printBuffer(const char *desc, UINT8_t *buff, UINT8_t len)
{
	if (buff) {
		int i;
		printf("%s:", desc);
		for (i=0; i<len; i++) {
			// if (i && i%40 == 0) printf("\n");
			if (i%8 == 0) printf("  ");
			printf("%02x", buff[i]);
		}
		printf("\n");
	}
	else printf("Buffer is empty.\n");
}

UINT32_t getValue(UINT8_t *buff, UINT8_t len)
{
	switch (len) {
		case 1:
			return buff[0];
		case 2:
			return myntohs(buff);
		case 4:
			return myntohl(buff);
	}

	return 0;
}

char* getStringForm(UINT8_t *buff, UINT16_t len)
{
	char *str;
	int i;

	str = malloc(len*2+1);
	if (str==NULL) {
		LOG(LOG_ERR, "Memory allocation for size %d byte failed.", len*2+1);
		return NULL;
	}

	for (i=0; i<len; i++) {
		sprintf(str+i*2, "%02X", buff[i]);
	}

	return str;
}

UINT8_t* parseString(const char *str)
{
	UINT8_t *buff;
	unsigned int buffLen, i;

	// TODO Parse string for odd string lengths
	if (str==NULL || strlen(str)%2) return NULL;

	buffLen = strlen(str)>>1;
	buff = malloc(buffLen);

	for (i=0; i<buffLen; i++) {
        sscanf(&str[2*i], "%2hhx", &buff[i]);
	}

	return buff;
}
