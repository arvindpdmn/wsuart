/*
 * main.c
 *
 *  Created on: Nov 13, 2013
 *      Author: AP
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <dis.h>
#include <gw.h>
#include <pktSched.h>
#include <pktProc.h>
#include <logger.h>
#include <util.h>
#include <wsuart.h>
#include <eventThread.h>
#include <main.h>

#ifdef API_ENA
#include <apiaccess.h>
CURLM *mainCon = NULL;
#else
#include <database.h>
MYSQL *mainCon = NULL;
#endif
unsigned int NwInitDone = 0;

#ifdef SMS_ENA
#include <sms.h>
#endif

int main(int argc, char **argv)
{
	if (argc<2) {
		LOG(LOG_ERR, "UART device name is expected.");
		exit(1);
	}

	// UART configuration
	if (cfgPort(argv[1]) < 0) return 1;

	// DB initializations
#ifdef API_ENA
	mainCon = initApi();
#else
	initDb();
	mainCon = openDbConnection();
#endif

	initSchedTxQueue();

#ifndef SNIFFER
	while(startCoord()) sleep(1);
	scheduleNwInfo();
	while (numPktsQueued()) {
		processTxQueue();
	}
	initAllNodeInfo();

	char *coordMacAddrStr = readCoordExtAddr(mainCon);
	UINT8_t *coordMacAddr = parseString(coordMacAddrStr);
	insertEvent(mainCon, coordMacAddr, "Info", "Boot", "Gateway finished initialization.");
	free(coordMacAddrStr);
	free(coordMacAddr);
#endif

#ifdef SMS_ENA
	GSM_initModem(GSM_modemSerDevName);
#endif

#ifndef API_ENA
	// Create thread to handle incoming events
	// When using API, web server takes care of pushing events when sensor data comes in
    pthread_t evtThread;
    thData evtData;
    pthread_create (&evtThread, NULL, (void *) &waitForEvents, (void *)&evtData);
#endif

    NwInitDone = 1; // now we are ready to process any packet

	// TODO Rather than polling, use interrupt driven rx and async tx
    while (1) {
        // Wait for packets and process them as received
    	UINT8_t *rxPkt = NULL;
		rxPkt = receivePkt();
		processPkt(NULL, rxPkt);
		free(rxPkt);

		// Process anything pending in transmit queue
		if (numPktsQueued()) { // only first packet
			processTxQueue();
		}
	}

#ifdef API_ENA
	endApi(mainCon);
#else
	closeDbConnection(mainCon);
	endDb();
#endif

	return 0;
}

