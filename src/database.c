#ifndef API_ENA

#define __USE_XOPEN
#define _GNU_SOURCE

#include <string.h>
#include <stdlib.h>

#include <database.h>
#include <logger.h>
#include <util.h>
#include <mac_defs.h>

static char* getNodeAssocState(MYSQL *con, UINT8_t value);

#ifndef REMOTE_DB
#define DB_HOST_ADDR "127.0.0.1"
#define DB_USER "root"
#define DB_PWD ""
#define DB_NAME "wisight"
#else
#define DB_HOST_ADDR "mysql.server261.com"
#define DB_USER "wwsn"
#define DB_PWD "Wisense@123"
#define DB_NAME "iedf_wisight"
#endif

#define DB_VER(v) #v
#define CMD_BUFF_LEN 1024

// TODO Have DB table definitions in a separate file instead of static array
#define NUM_DB_TABLES 11
DbTableDef dbTblDefs[NUM_DB_TABLES] =
{
	{	"WsnVerInfo", // ought to be the first table for code convenience
		"type ENUM('NetworkInfo', 'NodeInfo', 'NodeWhiteList', 'AddressMap', 'RouteInfo', 'SensorInfo', 'SensorData', 'EventInfo', 'Rule', 'PendingAction') NOT NULL, "
		"version VARCHAR(8) NOT NULL DEFAULT '" DB_VER(1.2) "', "
		"PRIMARY KEY (type)"
	},
	{	"NetworkInfo",
		"panId CHAR(4) NOT NULL DEFAULT '0000', "
		"nodeCount SMALLINT UNSIGNED NOT NULL DEFAULT 0, "
		"band VARCHAR(32) NOT NULL DEFAULT '-', "
		"channel INT UNSIGNED NOT NULL DEFAULT 0, "
		"modulation VARCHAR(32) NOT NULL DEFAULT '-', "
		"radioBaudRate INT UNSIGNED NOT NULL DEFAULT 0, "
		"coordMacAddr CHAR(16) NOT NULL DEFAULT '0000000000000000', "
		"coordTxPower SMALLINT NOT NULL DEFAULT 0, "
		"nodeAssocState ENUM('-', 'Enabled', 'Disabled') NOT NULL DEFAULT '-', "
		"PRIMARY KEY (panId)"
	},
	{
		"NodeInfo",
		"nodeUid SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, "
		"name VARCHAR(16) NOT NULL DEFAULT '-', "
		"location VARCHAR(16) NOT NULL DEFAULT '-',"
		"macAddr CHAR(16) NOT NULL, "
		"macShortAddr CHAR(4) NOT NULL DEFAULT '0000', "
		"panId CHAR(4) NOT NULL DEFAULT '0000', "
		"lastHopRssi TINYINT NOT NULL DEFAULT -128, "
		"lastHopCorr TINYINT UNSIGNED NOT NULL DEFAULT 0, " // TODO Update DB data type for lastHopCorr
		"pushPeriod SMALLINT UNSIGNED NOT NULL DEFAULT 0, "
		"macCapability CHAR(2) NOT NULL DEFAULT '00', "
		"powerSrc ENUM('Unknown', 'Mains', 'Battery', 'Solar') NOT NULL DEFAULT 'Unknown', "
		"batteryMAh SMALLINT UNSIGNED NOT NULL DEFAULT 0, "
		"state ENUM('Null', 'Associated', 'Disassociated', 'Active', 'Standby', 'NoReply', 'NoRoute') NOT NULL DEFAULT 'Null', "
		"radioPartNum VARCHAR(32) NOT NULL DEFAULT '-', "
		"buildDate DATE NOT NULL, "
		"buildTime TIME NOT NULL, "
		"modifyTs TIMESTAMP NOT NULL, "
		"PRIMARY KEY (nodeUid)"
	},
	{
		"NodeWhiteList",
		"macAddr CHAR(16) NOT NULL, "
		"panId CHAR(4) NOT NULL DEFAULT '0000', "
		"PRIMARY KEY (macAddr)"
	},
	{	// TODO Currently AddressMap is not used
		"AddressMap",
		"macShortAddr CHAR(4) NOT NULL, "
		"macAddr CHAR(16) NOT NULL, "
		"PRIMARY KEY (macShortAddr)"
	},
	{
		"RouteInfo",
		"nodeUid SMALLINT UNSIGNED NOT NULL, "
		"numHops TINYINT UNSIGNED NOT NULL DEFAULT 0, "
		"route VARCHAR(256) NOT NULL DEFAULT '-', "
		"PRIMARY KEY (nodeUid)"
	},
	{
		"SensorInfo",
		"sensorUid SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, "
		"nodeUid SMALLINT UNSIGNED NOT NULL, "
		"sensorId TINYINT UNSIGNED NOT NULL DEFAULT 0, "
		"manufacturer VARCHAR(16) NOT NULL DEFAULT '-', "
		"partNum VARCHAR(16) NOT NULL DEFAULT '-', "
		"activeTime VARCHAR(16) NOT NULL DEFAULT '-', "
		"activePwr VARCHAR(16) NOT NULL DEFAULT '-', "
		"standbyPwr VARCHAR(16) NOT NULL DEFAULT '-', "
		"opMode BINARY(1) NOT NULL DEFAULT '0', "
		"pushPeriod VARCHAR(16) NOT NULL DEFAULT '-', "
		"alarmPeriod VARCHAR(16) NOT NULL DEFAULT '-', "
		"measure ENUM('Instantaneous', 'Average', 'Cumulative') NOT NULL DEFAULT 'Instantaneous', "
		"mode ENUM('Unknown', 'Analog', 'Digital') NOT NULL DEFAULT 'Unknown', "
		"type ENUM('Unsupported', 'Voltage', 'Temperature', 'Motion', 'Switch', 'Pressure', 'Luminance', 'Moisture', 'Volume') NOT NULL DEFAULT 'Unsupported', "
		"unit ENUM('Unknown', 'Number', 'Counter', 'Volt', 'Celsius', 'Fahrenheit', 'Pascal', 'Lux', 'Inch', 'Percent', 'Litre') NOT NULL DEFAULT 'Unknown', "
		"scale ENUM('Tera', 'Giga', 'Mega', 'Kilo', 'Hecto', 'Deka', 'Unit', 'Deci', 'Centi', 'Milli', 'Micro', 'Nano', 'Pico', 'Femto') NOT NULL DEFAULT 'Unit', "
		"minVal SMALLINT NOT NULL DEFAULT 0, "
		"maxVal SMALLINT NOT NULL DEFAULT 0, "
		"thresholdLow SMALLINT NOT NULL DEFAULT 0, "
		"thresholdHigh SMALLINT NOT NULL DEFAULT 0, "
		"lastUpdate TIMESTAMP NOT NULL DEFAULT 0, "
		"nextUpdate TIMESTAMP NOT NULL DEFAULT 0, "
		"state ENUM('Unknown', 'Active', 'Standby', 'Shutdown', 'Error') NOT NULL DEFAULT 'Unknown', "
		"PRIMARY KEY (sensorUid)"
	},
	{
		"SensorData",
		"sensorUid SMALLINT UNSIGNED NOT NULL, "
		"ts TIMESTAMP NOT NULL DEFAULT NOW(), "
		"data MEDIUMINT NOT NULL DEFAULT 0"
	},
	{
		"EventInfo",
		"nodeUid SMALLINT UNSIGNED NOT NULL, "
		"ts TIMESTAMP NOT NULL DEFAULT NOW(), "
		"severity ENUM('Fatal', 'Error', 'Warn', 'Info') NOT NULL DEFAULT 'Info', "
		"type ENUM('Unknown', 'Boot', 'NodeCommissioning', 'NodeAssoc', 'NodeDisAssoc', 'NodeRouteDiscovery', 'PktFromNode', 'FatalErrId', 'RuleTrigger') NOT NULL DEFAULT 'Unknown', "
		"msg VARCHAR(128) NOT NULL DEFAULT '-'"
	},
	{
		"Rule",
		"ruleUid SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT, "
		"nodeUid SMALLINT UNSIGNED NOT NULL, "
		"sensorUid SMALLINT UNSIGNED NOT NULL DEFAULT 0, "
		"type ENUM('LowThreshold', 'HighThreshold') NOT NULL, "
		"threshold SMALLINT NOT NULL DEFAULT 0, "
		"actionOn ENUM('Gateway', 'Cloud') NOT NULL, "
		"notifyPeriod SMALLINT NOT NULL DEFAULT 0, "
		"notifiedTs TIMESTAMP NOT NULL DEFAULT NOW(), "
		"mobileNum VARCHAR(16) NOT NULL DEFAULT '', "
		"PRIMARY KEY (ruleUid)"
	},
	{
		"PendingAction",
		"ts TIMESTAMP NOT NULL DEFAULT NOW(), "
		"type ENUM('coord-reset', 'nj-ena', 'nj-dis', 'wl-add', 'wl-del', 'wl-clr', 'cfg-dpi', 'dioc') NOT NULL, "
		"data VARCHAR(1024) NOT NULL DEFAULT ''"
	}
};

static UINT16_t PanId; // TODO Support multiple PAN

void initDb(void)
{
	mysql_library_init(0, NULL, NULL);
}

void endDb(void)
{
	mysql_library_end();
}

MYSQL* openDbConnection(void)
{
	MYSQL *con;

	con = mysql_init(NULL);
	if (con == NULL)
	{
		printErrAndExit(con, __LINE__);
	}

	if (mysql_real_connect(con, DB_HOST_ADDR, DB_USER, DB_PWD,
			DB_NAME, 0, NULL, 0) == NULL)
	{
		printErrAndExit(con, __LINE__);
	}

	return con;
}

void closeDbConnection(MYSQL *con)
{
	mysql_close(con);
}

static MYSQL_RES* executeQueryWithResult(MYSQL *con, char *cmd, UINT8_t exitIfErr)
{
	LOG(LOG_DBG_MEDIUM, "%s", cmd);

	if (mysql_query(con, cmd)) {
		if (exitIfErr) printErrAndExit(con, __LINE__);
		else {
			printErr(con, __LINE__);
			return NULL;
		}
	}

	return mysql_store_result(con);
}

static UINT8_t executeQuery(MYSQL *con, char *cmd, UINT8_t exitIfErr)
{
	LOG(LOG_DBG_MEDIUM, "%s", cmd);

	if (mysql_query(con, cmd)) {
		if (exitIfErr) printErrAndExit(con, __LINE__);
		else {
			printErr(con, __LINE__);
			return 1;
		}
	}

	MYSQL_RES *result = mysql_store_result(con);
	mysql_free_result(result);

	return 0;
}

void printErrAndExit(MYSQL *con, unsigned int lineNum)
{
  LOG(LOG_FATAL, "%d: %s", lineNum, mysql_error(con));
  mysql_close(con);
  exit(1);
}

void printErr(MYSQL *con, unsigned int lineNum)
{
	LOG(LOG_ERR, "%d: %s", lineNum, mysql_error(con));
}

unsigned int getErrno(MYSQL *con)
{
	return mysql_errno(con);
}

UINT8_t isPresent(MYSQL *con, char *cmd)
{
	if (mysql_query(con, cmd)) {
		printErr(con, __LINE__);
		return 0; // return as if absent
	}

	MYSQL_RES *result;
	MYSQL_ROW row;
	UINT8_t isPresent = 0;

	result = mysql_store_result(con);
	if (result == NULL) printErrAndExit(con, __LINE__);

	int num_fields = mysql_num_fields(result);
	row = mysql_fetch_row(result);
	if (num_fields && atoi(row[0]) != 0) isPresent = 1;

	mysql_free_result(result);

	return isPresent;
}

UINT8_t updateNodeInfo(MYSQL *con, NodeInfo *nodeInfo_p)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(nodeInfo_p->macAddr, MAC_EXT_ADDR_LEN);

	UINT8_t inError = 0;

	// Change the state of other nodes having the same short address
	sprintf(cmdBuff, "UPDATE NodeInfo SET state='NULL' WHERE panId='%04X' AND macShortAddr='%04X' AND state!='NULL' AND macAddr!='%s'",
			PanId, nodeInfo_p->macShortAddr, macAddrStr);
	executeQuery(con, cmdBuff, 0);

	// TODO Update correctly number of sensors and other node information
	sprintf(cmdBuff, "SELECT COUNT(*) FROM NodeInfo WHERE macAddr='%s' LIMIT 1", macAddrStr);
	if (isPresent(con, cmdBuff)) {
		sprintf(cmdBuff, "UPDATE NodeInfo SET macShortAddr='%04X', panId='%04X', macCapability='%02X', state='Associated', modifyTs=NOW() WHERE macAddr='%s'",
				nodeInfo_p->macShortAddr, PanId, nodeInfo_p->macCapability, macAddrStr);
	}
	else {
		// Give default name and location at insert
		sprintf(cmdBuff, "INSERT INTO NodeInfo (name, location, macAddr, macShortAddr, panId, macCapability, pushPeriod, state) VALUES('%s', '%s', '%s', '%04X', '%04X', '%02X', %u, 'Associated')",
				macAddrStr+12, "Unknown", macAddrStr, nodeInfo_p->macShortAddr, PanId, nodeInfo_p->macCapability, nodeInfo_p->pushPeriod);
		inError = executeQuery(con, cmdBuff, 0);
		if (inError==0) {
			sprintf(cmdBuff, "SELECT nodeUid from NodeInfo WHERE panId='%04X' AND macAddr='%s' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1",
					PanId, macAddrStr);
			MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 1);
			MYSQL_ROW row = mysql_fetch_row(result);
			UINT16_t nodeUid = atoi(row[0]);
			mysql_free_result(result);
			sprintf(cmdBuff, "INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) VALUES(%u, NOW(), 'Info', 'NodeCommissioning', 'Node identified by its MAC address has been auto-commissioned.')", nodeUid);
		}
		else {
			free(macAddrStr);
			return inError;
		}
	}
	free(macAddrStr);

	return executeQuery(con, cmdBuff, 0);
}

UINT16_t readPushPeriod(MYSQL *con, UINT8_t *macAddr)
{
	char cmdBuff[CMD_BUFF_LEN];
	UINT16_t pushPeriod;

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "SELECT pushPeriod FROM NodeInfo WHERE panId='%04X' AND macAddr='%s' AND state!='NULL'", PanId, macAddrStr);
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 0);
	MYSQL_ROW row = mysql_fetch_row(result);

	sscanf(row[0], "%u", &pushPeriod);

	mysql_free_result(result);
	free(macAddrStr);

	return pushPeriod;
}

UINT8_t updateNodeSensors(MYSQL *con, UINT8_t *macAddr, SensorInfo *sensor_p)
{
	UINT8_t retVal=0;

	// Do nothing for now: sensors are added to DB when first data comes in

	return retVal;
}

UINT8_t updateSensorInfo(MYSQL *con, UINT8_t *macAddr, SensorInfo *sensor_p)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "SELECT COUNT(*) FROM SensorInfo WHERE "
			"nodeUid=(SELECT nodeUid from NodeInfo WHERE panId='%04X' AND macAddr='%s' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1) "
			"AND sensorId=%u",
			PanId, macAddrStr, sensor_p->sensorId);
	if (isPresent(con, cmdBuff)) {
		// TODO Update existing sensor information
		free(macAddrStr);
		return 1;
	}
	else {
		sprintf(cmdBuff, "INSERT INTO SensorInfo (nodeUid, sensorId, manufacturer, partNum, activeTime, activePwr, standbyPwr, "
				"opMode, pushPeriod, alarmPeriod, measure, mode, type, unit, scale, minVal, maxVal, thresholdLow, thresholdHigh, state) "
				"VALUES((SELECT nodeUid from NodeInfo WHERE panId='%04X' AND macAddr='%s' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1), '%u', '%s', '%s', '%s', '%s', '%s', "
				"'%u', '%s', '%s', '%s', '%s', '%s', '%s', '%s', "
				"'%d', '%d', '%d', '%d', '%s')",
				PanId, macAddrStr, sensor_p->sensorId, sensor_p->manufacturer, sensor_p->partNum, sensor_p->activeTime, sensor_p->activePwr, sensor_p->standbyPwr,
				sensor_p->opMode, sensor_p->pushPeriod, sensor_p->alarmPeriod, sensor_p->measure, sensor_p->mode, sensor_p->type, sensor_p->unit, sensor_p->scale,
				sensor_p->minVal, sensor_p->maxVal, sensor_p->thresholdLow, sensor_p->thresholdHigh, sensor_p->state);
		free(macAddrStr);
		return executeQuery(con, cmdBuff, 0);
	}
}

UINT8_t updatePanId(MYSQL *con, UINT16_t value)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "SELECT COUNT(*) FROM NetworkInfo WHERE panId='%04X'", value);
	if (!isPresent(con, cmdBuff)) {
		sprintf(cmdBuff, "INSERT INTO NetworkInfo (panId) VALUES('%04X')", value);
	}

	if (executeQuery(con, cmdBuff, 0)) {
		return 1;
	}
	else {
		PanId = value;
		return 0;
	}
}

UINT8_t updateNodeCount(MYSQL *con, UINT16_t value)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "UPDATE NetworkInfo SET nodeCount=%u WHERE panId='%04X'",
			value, PanId);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t updateBand(MYSQL *con, UINT8_t* str)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "UPDATE NetworkInfo SET band='%s' WHERE panId='%04X'",
			str, PanId);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t updateNwChannel(MYSQL *con, UINT32_t value)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "UPDATE NetworkInfo SET channel=%lu WHERE panId='%04X'",
			value, PanId);

	return executeQuery(con, cmdBuff, 0);
}

UINT16_t readChannelNumber(MYSQL *con)
{
	char cmdBuff[CMD_BUFF_LEN];
	UINT16_t channelNum;

	sprintf(cmdBuff, "SELECT channel FROM NetworkInfo WHERE panId='%04X'", PanId);
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 0);
	MYSQL_ROW row = mysql_fetch_row(result);

	if (atoi(row[0])<=BASE_FREQ) channelNum = 0;
	else channelNum = (atoi(row[0])-BASE_FREQ)/CHANNEL_SPACING; // Hz to channel number

	mysql_free_result(result);

	return channelNum;
}

UINT8_t updateModulation(MYSQL *con, UINT8_t* str)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "UPDATE NetworkInfo SET modulation='%s' WHERE panId='%04X'",
			str, PanId);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t updateRadioBaudRate(MYSQL *con, UINT32_t value)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "UPDATE NetworkInfo SET radioBaudRate=%lu WHERE panId='%04X'",
			value, PanId);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t updateCoordExtAddr(MYSQL *con, UINT8_t *macAddr)
{
	char cmdBuff[CMD_BUFF_LEN];
	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "SELECT COUNT(*) FROM NodeInfo WHERE macAddr='%s'", macAddrStr);
	if (!isPresent(con, cmdBuff)) {
		sprintf(cmdBuff, "INSERT INTO NodeInfo (name,macAddr,macShortAddr,panId,state) VALUES('LPWMN Coord','%s','%04X','%04X','Active')",
				macAddrStr, PAN_COORD_SHORT_ADDR, PanId);
		executeQuery(con, cmdBuff, 0);
	}

	sprintf(cmdBuff, "UPDATE NetworkInfo SET coordMacAddr='%s' WHERE panId='%04X'",
			macAddrStr, PanId);
	free(macAddrStr);

	return executeQuery(con, cmdBuff, 0);
}

UINT16_t readCoordUid(MYSQL *con)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "SELECT nodeUid FROM NodeInfo WHERE panId='%04X' AND macShortAddr='%04X'",
			PanId, PAN_COORD_SHORT_ADDR);
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 0);
	MYSQL_ROW row = mysql_fetch_row(result);
	UINT16_t coordUid = atoi(row[0]);
	mysql_free_result(result);
	return coordUid;
}

char* readCoordExtAddr(MYSQL *con)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "SELECT coordMacAddr FROM NetworkInfo WHERE panId='%04X'", PanId);
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 0);

	if (result==NULL) return NULL;
	if (result->row_count==0) {
		mysql_free_result(result);
		return NULL;
	}

	MYSQL_ROW row = mysql_fetch_row(result);

	char *macAddr = malloc(17);
	strncpy(macAddr, row[0], 16);
	macAddr[16] = 0;

	mysql_free_result(result);

	return macAddr;
}

char* readNodeExtAddr(MYSQL *con, UINT16_t macShortAddr)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "SELECT macAddr FROM NodeInfo WHERE panId='%04X' AND macShortAddr='%04X' AND state!='Null'", PanId, macShortAddr);
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 0);
	MYSQL_ROW row = mysql_fetch_row(result);
	char *macAddr = NULL;

	if (row) {
		macAddr = malloc(17);
		strncpy(macAddr, row[0], 16);
		macAddr[16] = 0;
	}

	mysql_free_result(result);

	return macAddr;
}

UINT8_t updateCoordTxPwr(MYSQL *con, UINT16_t value)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "UPDATE NetworkInfo SET coordTxPower=%d WHERE panId='%04X'",
			value, PanId);

	return executeQuery(con, cmdBuff, 0);
}

static char* getNodeAssocState(MYSQL *con, UINT8_t value)
{
	static char state[16];
	if (value) strcpy(state,"Enabled");
	else strcpy(state,"Disabled");
	return state;
}

UINT8_t updateNodeAssocState(MYSQL *con, UINT8_t value)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "UPDATE NetworkInfo SET nodeAssocState='%s' WHERE panId='%04X'",
			getNodeAssocState(con, value), PanId);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t readNodeAssocState(MYSQL *con)
{
	char cmdBuff[CMD_BUFF_LEN];
	UINT8_t nodeAssocState;

	sprintf(cmdBuff, "SELECT nodeAssocState FROM NetworkInfo WHERE panId='%04X'", PanId);
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 0);
	MYSQL_ROW row = mysql_fetch_row(result);

	if (strcmp(row[0],"Disabled")) nodeAssocState = 0;
	else nodeAssocState = 1;

	mysql_free_result(result);

	return nodeAssocState;
}

UINT8_t updateNodeBuildDate(MYSQL *con, UINT8_t *macAddr, char *value_p)
{
	char cmdBuff[CMD_BUFF_LEN], dateStr[16];
	struct tm dt;

	strptime(value_p, "%b %d %Y", &dt);
	strftime(dateStr, sizeof(dateStr), "%Y-%m-%d",&dt);

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "UPDATE NodeInfo SET buildDate='%s' WHERE panId='%04X' AND macAddr='%s'",
			dateStr, PanId, macAddrStr);

	free(macAddrStr);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t updateNodeBuildTime(MYSQL *con, UINT8_t *macAddr, char *value_p)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "UPDATE NodeInfo SET buildTime='%s' WHERE panId='%04X' AND macAddr='%s'",
			value_p, PanId, macAddrStr);

	free(macAddrStr);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t addToNodeWhiteList(MYSQL *con, UINT8_t *macAddr)
{
	LOG(LOG_WARN, "Call to addToNodeWhiteList() unexpected.");

	char cmdBuff[CMD_BUFF_LEN];
	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "SELECT COUNT(*) FROM NodeWhiteList WHERE macAddr='%s' AND panId='%04X'", macAddrStr, PanId);
	if (!isPresent(con, cmdBuff)) {
		sprintf(cmdBuff, "INSERT INTO NodeWhiteList (macAddr,panId) VALUES('%s','%04X')", macAddrStr, PanId);
		free(macAddrStr);
		return executeQuery(con, cmdBuff, 0);
	}
	else free(macAddrStr);

	return 0;
}

UINT8_t insertEvent(MYSQL *con, UINT8_t *macAddr, const char *severity, const char *type, char *msg)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "INSERT INTO EventInfo VALUES("
		"(SELECT nodeUid from NodeInfo WHERE panId='%04X' AND macAddr='%s' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1), "
		"NOW(), '%s', '%s', '%s')",
		PanId, macAddrStr, severity, type, msg);

	free(macAddrStr);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t updateRssiAndCorrelation(MYSQL *con, UINT8_t *macAddr, SINT8_t rssi, UINT8_t corr)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "UPDATE NodeInfo SET lastHopRssi=%d, lastHopCorr=%u, state='Active' WHERE panId='%04X' AND macAddr='%s' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1",
			rssi, corr, PanId, macAddrStr);

	free(macAddrStr);

	return executeQuery(con, cmdBuff, 0);
}

UINT8_t updateSensorData(MYSQL *con, UINT8_t *macAddr, UINT8_t sensorId, UINT32_t value)
{
	char cmdBuff[CMD_BUFF_LEN];
	UINT8_t retVal;

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);
	char *coordMacAddrStr = readCoordExtAddr(con);

	if (strncmp(macAddrStr,coordMacAddrStr,strlen(macAddrStr))==0) {
		LOG(LOG_WARN, "Sensor data from PAN coordinator is unexpected.");
		free(macAddrStr);
		free(coordMacAddrStr);
		return 1;
	}
	else {
		sprintf(cmdBuff,
			"UPDATE SensorInfo SET SensorInfo.state='Active', lastUpdate=NOW(), nextUpdate=DATE_ADD(NOW(), INTERVAL (SELECT pushPeriod FROM NodeInfo WHERE nodeUid=(SELECT nodeUid FROM NodeInfo WHERE panId='%04X' AND macAddr='%s' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1)) SECOND) "
			"WHERE SensorInfo.nodeUid=(SELECT nodeUid FROM NodeInfo WHERE panId='%04X' AND macAddr='%s' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1) AND sensorId=%u",
			PanId, macAddrStr, PanId, macAddrStr, sensorId);
		retVal = executeQuery(con, cmdBuff, 0);
		if (retVal)	{
			free(macAddrStr);
			free(coordMacAddrStr);
			return retVal;
		}

		sprintf(cmdBuff, "INSERT INTO SensorData VALUES("
			"(SELECT sensorUid FROM SensorInfo,NodeInfo WHERE SensorInfo.nodeUid=NodeInfo.nodeUid AND panId='%04X' AND macAddr='%s' AND NodeInfo.state!='NULL' AND sensorId=%u ORDER BY modifyTs DESC LIMIT 1), "
			"NOW(), '%ld')",
			PanId, macAddrStr, sensorId, (SINT32_t)value);
		free(macAddrStr);
		free(coordMacAddrStr);
		return executeQuery(con, cmdBuff, 0);
	}
}

UINT8_t updateRouteInfo(MYSQL *con, UINT8_t *macAddr)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "SELECT nodeUid from NodeInfo WHERE panId='%04X' AND macAddr='%s' AND state!='NULL' ORDER BY modifyTs DESC LIMIT 1",
			PanId, macAddrStr);
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 1);
	MYSQL_ROW row = mysql_fetch_row(result);
	UINT16_t nodeUid = atoi(row[0]);
	mysql_free_result(result);

	free(macAddrStr);

	UINT8_t inError = 0;

	sprintf(cmdBuff, "SELECT COUNT(*) FROM RouteInfo WHERE nodeUid=%u", nodeUid);
	if (isPresent(con, cmdBuff)) {
		UINT16_t coordUid = readCoordUid(con);
		sprintf(cmdBuff, "UPDATE RouteInfo SET route='%u-%u' WHERE nodeUid=%u",
				nodeUid, coordUid, nodeUid);
		inError = executeQuery(con, cmdBuff, 0);
	}
	else {
		UINT16_t coordUid = readCoordUid(con);
		sprintf(cmdBuff, "INSERT INTO RouteInfo (nodeUid, numHops, route) "
				"VALUES(%u, 1, '%u-%u')", nodeUid, nodeUid, coordUid);
		inError = executeQuery(con, cmdBuff, 0);
	}

	if (inError==0) {
		sprintf(cmdBuff, "INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) "
				"VALUES(%u, NOW(), 'Info', 'NodeRouteDiscovery', 'Node route has been received.')", nodeUid);
		return executeQuery(con, cmdBuff, 0);
	}

	return inError;
}

MYSQL_RES* readNodeWhiteList(MYSQL *con)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "SELECT macAddr FROM NodeWhiteList WHERE panId='%04X'", PanId);
	return executeQueryWithResult(con, cmdBuff, 0);
}

MYSQL_RES* readRule(MYSQL *con, UINT8_t *macAddr, UINT8_t sensorId)
{
	char cmdBuff[CMD_BUFF_LEN];

	char *macAddrStr = getStringForm(macAddr, MAC_EXT_ADDR_LEN);

	sprintf(cmdBuff, "SELECT SensorInfo.nodeUid,sensorUid FROM NodeInfo,SensorInfo WHERE panId='%04X' AND SensorInfo.nodeUid=NodeInfo.nodeUid AND macAddr='%s' AND NodeInfo.state='Active' AND sensorId=%d",
			PanId, macAddrStr, sensorId);
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 0);
	MYSQL_ROW row = mysql_fetch_row(result);
	sprintf(cmdBuff, "SELECT *,UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(notifiedTs) FROM Rule WHERE nodeUid=%s AND sensorUid=%s AND actionOn='Gateway'",
			row[0], row[1]);
	mysql_free_result(result);

	free(macAddrStr);

	return executeQueryWithResult(con, cmdBuff, 0);
}

UINT8_t updateRuleNotifiedTs(MYSQL *con, UINT16_t ruleUid)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "UPDATE Rule SET notifiedTs=NOW() WHERE ruleUid=%d",
			ruleUid);

	return executeQuery(con, cmdBuff, 0);
}

#ifdef GUI_INTERFACE_DB
PendingAction* getFirstPendingAction(MYSQL *con)
{
	char cmdBuff[CMD_BUFF_LEN];

	sprintf(cmdBuff, "SELECT type,data FROM PendingAction ORDER BY ts ASC LIMIT 1");
	MYSQL_RES *result = executeQueryWithResult(con, cmdBuff, 0);

	if (result==NULL) return NULL;
	if (result->row_count==0) {
		mysql_free_result(result);
		return NULL;
	}

	MYSQL_ROW row = mysql_fetch_row(result);
	PendingAction *action = malloc(sizeof(PendingAction));
	strncpy(action->type, row[0], sizeof(action->type));
	strncpy(action->data, row[1], sizeof(action->data));
	mysql_free_result(result);

	return action;
}

UINT8_t deleteFirstPendingAction(MYSQL *con)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "DELETE FROM PendingAction ORDER BY ts ASC LIMIT 1");
	return executeQuery(con, cmdBuff, 0);
}

UINT8_t clearAllPendingActions(MYSQL *con)
{
	char cmdBuff[CMD_BUFF_LEN];
	sprintf(cmdBuff, "DELETE FROM PendingAction");
	return executeQuery(con, cmdBuff, 0);
}

#endif

#endif
