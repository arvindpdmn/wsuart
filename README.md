# Overview #

This gateway application waits for packets on a USB port and processes those packets. These packets are received from a Low Power Wireless Mesh Network (LPWMN) coordinator node. The packets might originate from the network coordinator or from any other node and routed via the coordinator. The application can also send commands and requests to any node on the network and process responses.

The code can alternatively be compiled for a sniffer. The sniffer simply monitors traffic on the air interface and reports what's happening. It is a useful debugging tool.

# Installation #
## Ubuntu ##
* This is a simple C program that can be compiled using `gcc`.
* To install MySQL client library, run `sudo apt-get install libmysqlclient-dev`. This is required to save the data into MySQL database.
* To use API calls instead of directly accessing the database, cURL is required. To install this, run `sudo apt-get install curl libcurl libcurl-dev`.
* To process JSON responses in C code, `Jansson` is being used. To build and install this, follow instructions from [Jansson documentation](http://jansson.readthedocs.io).
* Program can be called (for example) as `sudo wsuart /dev/ttyUSB0`

## Windows ##
* Code must be compiled using `gcc` that comes with Cygwin. Note that MinGW is not sufficient to compile the code on Windows since MinGW is not POSIX compliant.
* You will need to update your Cygwin installation to include MySQL (dev), cURL (dev) and diffutils libraries.
* To process JSON responses in C code, `Jansson` is being used. To build and install this, follow instructions from [Jansson documentation](http://jansson.readthedocs.io).
* As an administrator, add the Cygwin directory to the environment path variable. This may be for example `C:\cygwin\bin`. This is necessary because Cygwin DLL is required to execute the program.
* When launching a terminal to run the program, launch it with administrative privileges. Otherwise, program will fail to open the USB port.
* Program can be called (for example of COM20) as `sudo wsuart /dev/ttyS19`

# Project Files #
We provide the following project files as a starting point but do note that we may not have updated some of them:

* Makefile: A basic makefile to compile the project without any IDE
* Eclipse: Files `.cproject`, `.project` and `.pydevproject`
* Visual Studio Code: Folder `.vscode`

# Developer Notes #
* The following preprocessor symbols are to be noted:
	* `API_CACHE_ENA`: If defined, responses to GET calls are cached to improve performance. This makes sense only when `API_ENA` is on.
	* `API_ENA`: If defined, use RESTful API access instead of accessing DB directly.
	* `GUI_INTERFACE_DB`: Web client GUI interfaces with this program via the database. If not defined, TCP/IP is used as the interface.
	* `RADIO_BAUD_RATE_1200`: Must be defined. It specifies a radio baud rate of 1200 symbols per second.
	* `RADIO_CC1101`: Must be defined. It specifies TI's CC1101 radio module.
	* `REMOTE_DB`: Use remote DB rather than local DB.
	* `SMS_ENA`: Triggers SMS notifications when thresholds are exceeded.
	* `SNIFFER`: If defined, code is compiled for a sniffer.
* The following include paths must be added in your IDE settings:
	* `{projectPath}/inc`
	* `{projectPath}/inc/imports`
* Makefile has these main targets:
	* `apigw` (Default): WiSight/DB resides on gateway and API calls will be used.
	* `gw`: WiSight/DB resides on gateway and DB calls will be used.
	* `apicloud`: WiSight/DB resides on cloud and API calls will be used.
	* `cloud`: WiSight/DB resides on cloud and DB calls will be used.
* Database access is coded in `src/database.c`. This has code for both gateway DB (local) and cloud DB (remote).

# Limitations (22-Apr-2015) #
* TCP/IP port is opened for listening to commands triggered by the user from GUI. However, this code is empty at the moment and nothing is processed. Currently, GUI interfaces to this program via the database.
* C implementation for SMS notifications from the gateway is not tested. For now, a Python implementation is provided.
* Many other limitations are noted in code as `TODO` comments.

# Licensing #
* Please refer to the file named `UNLICENSE` for details.
