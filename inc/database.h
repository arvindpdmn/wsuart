#ifndef DATABASE_H_
#define DATABASE_H_

#include <mysql/mysql.h>

#include <pktProc.h>
#include <eventThread.h>
#include <sensor.h>

typedef struct
{
	char *tblName;
	char *fieldsAndTypes;
} DbTableDef;

extern void initDb(void);
extern void endDb(void);
extern MYSQL* openDbConnection(void);
extern void closeDbConnection(MYSQL *con);
extern void printErrAndExit(MYSQL *con, unsigned int lineNum);
extern void printErr(MYSQL *con, unsigned int lineNum);
extern unsigned int getErrno(MYSQL *con);
extern UINT8_t isPresent(MYSQL *con, char *cmd);
extern UINT8_t updateNodeInfo(MYSQL *con, NodeInfo *nodeInfo_p);
extern UINT16_t readPushPeriod(MYSQL *con, UINT8_t *macAddr);
extern UINT8_t updateNodeSensors(MYSQL *con, UINT8_t *macAddr, SensorInfo *sensor_p);
extern UINT8_t updateSensorInfo(MYSQL *con, UINT8_t *macAddr, SensorInfo *sensor_p);
extern UINT8_t updatePanId(MYSQL *con, UINT16_t value);
extern UINT8_t updateNodeCount(MYSQL *con, UINT16_t value);
extern UINT8_t updateBand(MYSQL *con, UINT8_t* str);
extern UINT8_t updateNwChannel(MYSQL *con, UINT32_t value);
extern UINT16_t readChannelNumber(MYSQL *con);
extern UINT8_t updateModulation(MYSQL *con, UINT8_t* str);
extern UINT8_t updateRadioBaudRate(MYSQL *con, UINT32_t value);
extern UINT8_t updateCoordExtAddr(MYSQL *con, UINT8_t *macAddr);
extern UINT16_t readCoordUid(MYSQL *con);
extern char* readCoordExtAddr(MYSQL *con);
extern char* readNodeExtAddr(MYSQL *con, UINT16_t macShortAddr);
extern UINT8_t updateCoordTxPwr(MYSQL *con, UINT16_t value);
extern UINT8_t updateNodeAssocState(MYSQL *con, UINT8_t value);
extern UINT8_t readNodeAssocState(MYSQL *con);
extern UINT8_t updateNodeBuildDate(MYSQL *con, UINT8_t *macAddr, char *value_p);
extern UINT8_t updateNodeBuildTime(MYSQL *con, UINT8_t *macAddr, char *value_p);
extern UINT8_t addToNodeWhiteList(MYSQL *con, UINT8_t *macAddr);
extern UINT8_t insertEvent(MYSQL *con, UINT8_t *macAddr, const char *severity, const char *type, char *msg);
extern UINT8_t updateRssiAndCorrelation(MYSQL *con, UINT8_t *macAddr, SINT8_t rssi, UINT8_t corr);
extern UINT8_t updateSensorData(MYSQL *con, UINT8_t *macAddr, UINT8_t sensorId, UINT32_t value);
extern UINT8_t updateRouteInfo(MYSQL *con, UINT8_t *macAddr);
extern MYSQL_RES* readNodeWhiteList(MYSQL *con);
extern MYSQL_RES* readRule(MYSQL *con, UINT8_t *macAddr, UINT8_t sensorId);
extern UINT8_t updateRuleNotifiedTs(MYSQL *con, UINT16_t ruleUid);

#ifdef GUI_INTERFACE_DB
extern PendingAction* getFirstPendingAction(MYSQL *con);
extern UINT8_t deleteFirstPendingAction(MYSQL *con);
extern UINT8_t clearAllPendingActions(MYSQL *con);
#endif

#endif /* DATABASE_H_ */
