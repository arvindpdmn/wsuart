/*
 * sms.h
 *
 *  Created on: Apr 7, 2015
 *      Author: AP
 */

#ifndef SMS_H_
#define SMS_H_

extern char GSM_modemSerDevName[];

extern int GSM_initModem(char *modemPortStr_p);
extern int GSM_sendSMS(char *msg, const char *mobileNum);

#endif /* SMS_H_ */
