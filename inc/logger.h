/*
 * logger.h
 *
 *  Created on: Nov 14, 2013
 *      Author: AP
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <stdio.h>
#include <util.h>

#define LOG_FATAL		(1)
#define LOG_ERR			(2)
#define LOG_WARN		(3)
#define LOG_INFO		(4)
#define LOG_DBG_HIGH	(5)
#define LOG_DBG_MEDIUM	(6)
#define LOG_DBG_LOW		(7)

#define LOG(level, ...) do {  \
                            if (level <= debug_level) { \
                                fprintf(stdout,"%s:%d:", __FILE__, __LINE__); \
                                fprintf(stdout, __VA_ARGS__); \
                                fprintf(stdout, "\n"); \
                                fflush(stdout); \
                            } \
                        } while (0)

#define PRINT_BUFFER(level, a, b, c) \
						do {  \
                            if (level <= debug_level) { \
                            	printBuffer(a, b, c); \
							} \
                        } while (0)

extern int  debug_level;

#endif /* LOGGER_H_ */
