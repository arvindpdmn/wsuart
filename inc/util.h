/*
 * util.h
 *
 *  Created on: Nov 13, 2013
 *      Author: AP
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <typedefs.h>

#define FREE(x) { if(x) {free(x); x=NULL;} }

extern UINT8_t *myhtons(UINT8_t *buff_p, UINT16_t val);
extern UINT16_t myntohs(UINT8_t *buff_p);
extern UINT32_t myntohl(UINT8_t *buff_p);
extern UINT8_t *myhtonl(UINT8_t *buff_p, UINT32_t val);
extern void printBuffer(const char *desc, UINT8_t *buff, UINT8_t len);
extern UINT32_t getValue(UINT8_t *buff, UINT8_t len);
extern char* getStringForm(UINT8_t *buff, UINT16_t len);
extern UINT8_t* parseString(const char *str);

#endif /* UTIL_H_ */
