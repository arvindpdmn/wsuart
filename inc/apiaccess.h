#ifndef APIACCESS_H_
#define APIACCESS_H_

#include <jansson.h>
#include <curl/curl.h>

#include <pktProc.h>
#include <eventThread.h>
#include <sensor.h>

typedef struct string {
  char *ptr;
  size_t len;
} String;

extern CURLM* initApi(void);
extern void endApi(CURLM *mcurl);
extern void addHandle(CURLM *mcurl, CURL *curl);
extern void removeHandle(struct CURLMsg *msg);
extern void printErrAndExit(CURLcode res, unsigned int lineNum);
extern void printErr(CURLcode res, unsigned int lineNum);
extern UINT8_t isPresent(CURLM *mcurl, char *cmd);
extern UINT8_t updateNodeInfo(CURLM *mcurl, NodeInfo *nodeInfo_p);
extern UINT16_t readPushPeriod(CURLM *mcurl, UINT8_t *macAddr, UINT8_t useCache);
extern UINT8_t updateNodeSensors(CURLM *mcurl, UINT8_t *macAddr, SensorInfo *sensor_p);
extern UINT8_t updateSensorInfo(CURLM *mcurl, UINT8_t *macAddr, SensorInfo *sensor_p);
extern UINT8_t updatePanId(CURLM *mcurl, UINT16_t value);
extern UINT8_t updateNodeCount(CURLM *mcurl, UINT16_t value);
extern UINT8_t updateBand(CURLM *mcurl, UINT8_t* str);
extern UINT8_t updateNwChannel(CURLM *mcurl, UINT32_t value);
extern UINT16_t readChannelNumber(CURLM *mcurl);
extern UINT8_t updateModulation(CURLM *mcurl, UINT8_t* str);
extern UINT8_t updateRadioBaudRate(CURLM *mcurl, UINT32_t value);
extern UINT8_t updateCoordExtAddr(CURLM *mcurl, UINT8_t *macAddr);
extern char* readCoordExtAddr(CURLM *mcurl);
extern char* readNodeExtAddr(CURLM *mcurl, UINT16_t macShortAddr);
extern UINT8_t updateCoordTxPwr(CURLM *mcurl, UINT16_t value);
extern UINT8_t updateNodeAssocState(CURLM *mcurl, UINT8_t value);
extern UINT8_t readNodeAssocState(CURLM *mcurl);
extern UINT8_t updateNodeBuildDate(CURLM *mcurl, UINT8_t *macAddr, char *value_p);
extern UINT8_t updateNodeBuildTime(CURLM *mcurl, UINT8_t *macAddr, char *value_p);
extern UINT8_t addToNodeWhiteList(CURLM *mcurl, UINT8_t *macAddr);
extern UINT8_t insertEvent(CURLM *mcurl, UINT8_t *macAddr, const char *severity, const char *type, char *msg);
extern UINT8_t updateRssiAndCorrelation(CURLM *mcurl, UINT8_t *macAddr, SINT8_t rssi, UINT8_t corr);
extern UINT8_t updateAllSensorData(CURLM *mcurl, UINT8_t *macAddr, SINT8_t rssi, UINT8_t corr, UINT8_t numSensors, SensorDataStore *ds);
extern UINT8_t updateSensorData(CURLM *mcurl, UINT8_t *macAddr, UINT8_t sensorId, UINT32_t value);
extern UINT8_t updateRouteInfo(CURLM *mcurl, UINT8_t *macAddr);
extern char* readNodeWhiteList(CURLM *mcurl);
extern String* readRule(CURLM *mcurl, UINT8_t *macAddr);
extern UINT8_t updateRuleNotifiedTs(CURLM *mcurl, UINT16_t ruleUid);
extern UINT8_t getAllPendingActions(CURLM *mcurl, unsigned char *numActions, PendingAction **actions);

#endif /* APIACCESS_H_ */
