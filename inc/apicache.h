#ifndef APICACHE_H_
#define APICACHE_H_

#include <stdbool.h>
#include <string.h>
#include <mac_defs.h>

#define MAX_CACHE_LEN 64

typedef struct {
  UINT16_t panId;
  bool isActive;
  char getParams[1024];
  char rspStr[10240];
  unsigned int hits;
} CacheEntry;

extern void initApiCache(void);
extern const char* getFromApiCache(UINT16_t panId, const char *getParams);
extern void addToApiCache(UINT16_t panId, const char *getParams, const char *rspStr);
extern void removeFromApiCache(UINT16_t panId, const char *getParams);

#endif
