/*
 * eventThread.h
 *
 *  Created on: Nov 14, 2013
 *      Author: AP
 */

#ifndef EVENTTHREAD_H_
#define EVENTTHREAD_H_

#include <pthread.h>

typedef struct
{
	int placeholder;
} thData;

#ifdef GUI_INTERFACE_DB
typedef struct
{
	char type[32];
	char data[1024];
} PendingAction;
#endif

void processAction(PendingAction *pa);
void waitForEvents(void *ptr);

#endif /* EVENTTHREAD_H_ */
