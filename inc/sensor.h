/*
 * sensor.h
 *
 *  Created on: Apr 8, 2015
 *      Author: AP
 */

#ifndef SENSOR_H_
#define SENSOR_H_

#include <typedefs.h>
#include <dis.h>

#define NUM_SENSORS 8

#define PLTFRM_DUMMY_DEV_ID        0x0
#define PLTFRM_AT24C01C_1_DEV_ID    0x1           // I2C
#define PLTFRM_AT24MAC602_1_DEV_ID    0x2
#define PLTFRM_M24M01_1_DEV_ID  0x4
#define PLTFRM_LM75B_1_DEV_ID       0x9            // I2C
#define PLTFRM_TMP102_1_DEV_ID      0xa            // I2C
#define PLTFRM_NTC_THERMISTOR_1_DEV_ID   0xb       // Vishay - NTCALUG02A (analog)
#define PLTFRM_NTC_THERMISTOR_2_DEV_ID   0xc       // Semtech - 103AT_4 (analog)
#define PLTFRM_SFH_7773_1_DEV_ID    0x11           // I2C
#define PLTFRM_TSL45315_1_DEV_ID    0x12           // I2C
#define PLTFRM_MPU6050_1_DEV_ID     0x13           // I2C
#define PLTFRM_ADXL345_1_DEV_ID     0x14           // I2C and SPI
#define PLTFRM_SHT10_1_DEV_ID       0x15           // I2C
#define PLTFRM_LIS3MDLTR_1_DEV_ID   0x16           // I2C
#define PLTFRM_CC2520_1_DEV_ID      0x19           // SPI
#define PLTFRM_CC1200_1_DEV_ID      0x1a           // SPI
#define PLTFRM_CC1101_1_DEV_ID      0x1b           // SPI
#define PLTFRM_CC3100_1_DEV_ID      0x1c           // SPI
#define PLTFRM_MPL115A1_1_DEV_ID    0x20           // SPI
#define PLTFRM_MAX_SONAR_1_DEV_ID   0x28
#define PLTFRM_EKMC160111X_1_DEV_ID   0x30         // GPIO
#define PLTFRM_LED_1_DEV_ID           0x40         // GPIO
#define PLTFRM_LED_2_DEV_ID           0x41         // GPIO
#define PLTFRM_REED_SWITCH_1_DEV_ID   0x50         // GPIO
#define PLTFRM_SPST_SWITCH_1_DEV_ID   0x51         // GPIO
#define PLTFRM_MP3V5050GP_1_DEV_ID    0x60         // Analog input
#define PLTFRM_HE055T01_1_DEV_ID      0x61         // Analog input
#define PLTFRM_MP3V5004GP_1_DEV_ID    0x62         // Analog input
#define PLTFRM_LLS_1_DEV_ID           0x65
#define PLTFRM_BATT_1_DEV_ID          0x68         // Analog input
#define PLTFRM_AD7797_1_DEV_ID        0x70
#define PLTFRM_ON_CHIP_VCC_SENSOR_DEV_ID    0x78      // ADC_10 channel
#define PLTFRM_ON_CHIP_TEMP_SENSOR_DEV_ID    0x79      // ADC_10 channel
#define PLTFRM_SYNC_RT_1_DEV_ID  0x80
#define PLTFRM_GPIO_REMOTE_CTRL_DEV_ID  0x90
#define PLTFRM_CHIRP_PWLA_1_DEV_ID  0x91
#define PLTFRM_FC_28_1_DEV_ID  0x92  // Analog (soil moisture sensor)
#define PLTFRM_WSMS100_1_DEV_ID  0x93
#define PLTFRM_CC2D33S_1_DEV_ID    0xb0  // I2C
#define PLTFRM_MPL3115A2_1_DEV_ID  0xb8  // I2C
#define PLTFRM_MPL115A2_1_DEV_ID  0xb9  // I2C
#define PLTFRM_BMP180_1_DEV_ID  0xba  // I2C
#define PLTFRM_MMA7660FC_1_DEV_ID  0xc0  // I2C
#define PLTFRM_I2C_SW_BUS_1_DEV_ID  0xe0
#define PLTFRM_I2C_SW_BUS_2_DEV_ID  0xe1
#define PLFRM_SPI_HW_BUS_1_DEV_ID   0xe8
#define PLTFRM_32KHZ_CRYSTAL_DEV_ID  0xfe
#define PLTFRM_GENERIC_DEV_ID      0xff
#define PLTFRM_UART_HW_1_DEV_ID    0xa0

#define NTC_THERM_103AT_4_R25_VAL  10000
#define NTC_THERM_103AT_4_B_25_85_VAL   3435
#define NTC_THERM_NTCALUG02A_R25_VAL  10000
#define NTC_THERM_NTCALUG02A_B_25_85_VAL   3984

typedef struct
{
	UINT8_t sensorId;
	SINT8_t manufacturer[16];
	SINT8_t partNum[16];
	SINT8_t activeTime[16];
	SINT8_t activePwr[16];
	SINT8_t standbyPwr[16];
	UINT8_t opMode;
	SINT8_t pushPeriod[16];
	SINT8_t alarmPeriod[16];
	SINT8_t measure[16];
	SINT8_t mode[16];
	SINT8_t type[16];
	SINT8_t unit[16];
	SINT8_t scale[16];
	SINT16_t minVal;
	SINT16_t maxVal;
	SINT16_t thresholdLow;
	SINT16_t thresholdHigh;
	SINT8_t state[16];
} SensorInfo;

typedef struct
{
	DIS_sensorType_t type;
	DIS_dataScale_t scale;
	UINT8_t *value_p;
	UINT8_t valLen;
} SensorData;

typedef struct
{
	DIS_sensorType_t type;
	UINT32_t convertedValue;
} SensorDataStore;

extern SensorInfo fixedSensors[];

extern SensorInfo *getSensorInfo(UINT8_t sensorId);
extern UINT32_t convertSensorValue(UINT8_t sensorId, UINT32_t value, UINT32_t refVcc);

#endif /* SENSOR_H_ */
